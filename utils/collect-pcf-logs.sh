#!/bin/bash

# TODO: add proper signal handling

CF_APP_GUID=`cf app mediareleases --guid`
MEDIA_RELEASES_PCF_LOG="`date +%Y-%m-%d`-mediareleases-${CF_APP_GUID}-pcf.out"
echo "saving log to: $MEDIA_RELEASES_PCF_LOG"

while :; do
    # TODO: capture and save the return code of cf logs
    cf logs mediareleases | tee -a $MEDIA_RELEASES_PCF_LOG
    echo "interrupted connection to PCF" >> $MEDIA_RELEASES_PCF_LOG
done
