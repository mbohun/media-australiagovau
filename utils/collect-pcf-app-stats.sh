#!/bin/bash

# TODO: add proper signal handling

DATA_COLUMN_SEPARATOR='   '

CF_APP_GUID=`cf app mediareleases --guid`
MEDIA_RELEASES_APP_STATS_PCF_LOG="`date +%Y-%m-%d`-mediareleases-${CF_APP_GUID}-stats_pcf_ts.out"
echo "saving app stats to: $MEDIA_RELEASES_APP_STATS_PCF_LOG"

while :; do
    # TODO: capture and save the return code of cf logs
    time_now=`date "+%Y-%m-%d %T"`
    log_msg=`cf app mediareleases | grep -e '^#'`
    echo "${time_now}${DATA_COLUMN_SEPARATOR}${log_msg}" >> $MEDIA_RELEASES_APP_STATS_PCF_LOG
    sleep 20s
done
