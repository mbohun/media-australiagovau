#! /bin/bash

dehumanise() {
    for v in "$@"
    do
        echo $v | awk \
                      'BEGIN{IGNORECASE = 1}
       function printpower(n,b,p) {printf "%u\n", n*b^p; next}
       /[0-9]$/{print $1;next};
       /K(iB)?$/{printpower($1,  2, 10)};
       /M(iB)?$/{printpower($1,  2, 20)};
       /G(iB)?$/{printpower($1,  2, 30)};
       /T(iB)?$/{printpower($1,  2, 40)};
       /KB$/{    printpower($1, 10,  3)};
       /MB$/{    printpower($1, 10,  6)};
       /GB$/{    printpower($1, 10,  9)};
       /TB$/{    printpower($1, 10, 12)}'
    done
}

# timeseries line/record explanation
#
# ts_day    |ts_time |inst|status   |  st_day    |st_time |AP|CPU   |mem   |  |  |        |  |  |
# ----------+--------+----+---------+------------+--------+--+------+------+--+--+--------+--+--+
# 2016-10-13 19:18:11   #0   running   2016-10-06 06:26:53 PM   1.1%   1.2G of 2G   252.6M of 1G
#

cat $1 | sed -e 's/%//g' | while read -r ts_day ts_time inst status start_day start_time am_or_pm cpu mem crap1 mem_max hdd crap2 hdd_max
do
    # some check if we got back any data (the time-stamp at teh beginning of each line is created locally,
    # not by cf-cli)
    echo "$ts_day $ts_time,$cpu,`dehumanise $mem`,`dehumanise $hdd`"
done
