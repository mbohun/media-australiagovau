set datafile separator ","
set terminal pngcairo font "arial,8" size 5000,512
#set terminal svg font "arial,10" size 5000,250
#set title "http://media.australia.gov.au mem & cpu usage timeseries graph"

set xlabel "date"
set xdata time
set xtics "2016-09-28 00:00:00",86400
#set xrange ["2016-10-08 14:38:24":"2016-10-09 13:31:31"]
#
#set timefmt "%s"
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%Y-%m-%d %H:%M:%S"
set key left top
set grid

set multiplot layout 2,1 title "https://media.australia.gov.au"

set style line 1 lt 1 lw 6

set ylabel "mem usage"
set yrange [0:2147483648]
plot data_file using 1:3 title 'mem' with impulses, "marker.dat" using 1:3 title 'START' with impulses lw 6

set ylabel "cpu usage"
set yrange [0:100]
plot data_file using 1:2 title 'cpu' with impulses, "marker.dat" using 1:2 title 'START' with impulses lw 6

unset multiplot
