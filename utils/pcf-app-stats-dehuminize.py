import sys

def dehumanize(v):
    handlers = {
        "G" : 3,
        "M" : 2,
        "K" : 1
    }

    unit = v[len(v) - 1]
    val  = float(v.strip(unit))
    return int(round(val * 1024**handlers[unit]))

def process_file(file_name):
    print file_name

    f = open(file_name, 'r')
    lines = f.read().split('\n')
    
    for ln in lines:
        frags = ln.split()
        if len(frags) > 2:
            #                             timestamp         , CPU                 , mem                 , hdd
            print '{} {},{},{},{}'.format(frags[0], frags[1], frags[7].rstrip('%'), dehumanize(frags[8]), dehumanize(frags[11]))

if __name__=="__main__":
    process_file(sys.argv[1])
