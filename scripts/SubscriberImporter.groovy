import grails.converters.JSON
import groovy.json.JsonSlurper
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import subscriberpoc.Subscriber

//def base = "https://media.australia.gov.au/api/"
def base = "https://test.media.australia.gov.au/api/"
//def base = "http://sandbox/mediarelease/api/"
def topicsAPI = "topics"
def subscriberAPI = "subscribers"

URL topicAPIUrl = new URL(base + topicsAPI);

def topics = new JsonSlurper().parseText(topicAPIUrl.text)
def topicIds = []
topics.each { t ->
    topicIds.add(t.id)
}
println "Got Topics " + topics
def count = 1;
new File('scripts/import/subscribers.csv').splitEachLine(",") {fields ->
    println "Adding user [" + fields[0] + "]"
    count++
    def http = new HTTPBuilder(base + subscriberAPI)
    http.request( Method.POST, ContentType.JSON ) {
        uri.path = base + subscriberAPI
        body = [accepted: true, topics: topicIds, email: fields[0]]
        requestContentType = ContentType.JSON

        response.success = {resp ->
            println "Success! ${resp.status} [" + count + "]"
        }
        response.failure = {resp ->
            println "Request failed with status ${resp.status}"
        }
    }

}
