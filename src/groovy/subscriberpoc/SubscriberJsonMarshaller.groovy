package subscriberpoc

import grails.converters.JSON

/**
 * Created by dean on 1/09/15.
 */
class SubscriberJsonMarshaller {

    void register() {
        println "Inside separate subscribermarshaller"

        JSON.createNamedConfig("v1") { cfg ->
            cfg.registerObjectMarshaller(Subscriber) { Subscriber s ->
                return [
                        id         : s.id,
                        email      : s.email,
                        confirmCode: s.confirmCode,
                        verified   : false,
                        topics     : s.topics.collect { it.name }

                ]

            }

        }





    }

}
