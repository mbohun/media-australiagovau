package subscriberpoc

import grails.converters.XML

/**
 * Created by dean on 2/09/15.
 */
class TopicXmlMarshaller {

    void register(){
        println "Inside separate topic XML marshaller"

        XML.registerObjectMarshaller(Topic){Topic t->
            return [
                    id: t.id,
                    name: t.name,
                    description: t.description
            ]

        }
    }
}
