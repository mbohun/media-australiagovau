package subscriberpoc

import grails.converters.JSON

/**
 * Created by dean on 1/09/15.
 */
class TopicJsonMarshaller {

    void register() {
        println "Inside separate topic JSON marshaller"

        JSON.createNamedConfig("v1") { cfg ->
            cfg.registerObjectMarshaller(Topic){Topic t ->
                return [
                        id: t.id,
                        name: t.name,
                        description: t.description
                ]

            }
        }

    }
}
