package subscriberpoc

import grails.converters.XML

/**
 * Created by dean on 2/09/15.
 */
class SubscriberXmlMarshaller {

    void register() {

        println "Inside separate subscriber XML marshaller"


        XML.registerObjectMarshaller(Subscriber){Subscriber s->
            return [
                    id         : s.id,
                    email      : s.email,
                    confirmCode: s.confirmCode,
                    verified   : false,
                    topics     : s.topics.collect { it.name }
            ]

        }

    }
}


