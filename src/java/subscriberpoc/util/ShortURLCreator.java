package subscriberpoc.util;

public interface ShortURLCreator {
    abstract public String create(final String longUrl);
}
