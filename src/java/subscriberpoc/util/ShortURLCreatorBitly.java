package subscriberpoc.util;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

import net.swisstech.bitly.BitlyClient;
import net.swisstech.bitly.model.Response;
import net.swisstech.bitly.model.v3.ShortenResponse;

public class ShortURLCreatorBitly implements ShortURLCreator {

    private static final Log log = LogFactory.getLog(ShortURLCreatorBitly.class);

    private final BitlyClient bitly;

    public ShortURLCreatorBitly(final String accessToken) {
        this.bitly = new BitlyClient(accessToken);
    }

    public final String create(final String longUrl) {
        try {

            final Response<ShortenResponse> bitlyRes =
                this.bitly.shorten().setLongUrl(longUrl).call();

            final String bitlyUrl = bitlyRes.data.url;
            log.debug(longUrl + " => " + bitlyUrl);

            return bitlyUrl;

        } catch (Exception e) {
            log.debug("FAILED to create short Bitly URL for: " + longUrl);
            e.printStackTrace();
            return null;
        }
    }
}
