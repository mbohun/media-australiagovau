```
# start the vm
cd installer/vagrant/centos; vagrant up

# scp a db dump into the vm to: /tmp/db1_dump.sql (this where the install_db.sh script expects it)
scp /home/mbohun/Downloads/db1_dump-2016-06-28.sql vagrant@10.1.1.2:/tmp/db1_dump.sql

# run the install_db.sh script this should install mariadb, and setup the db for use
cat ./install_db.sh | ssh -t vagrant@10.1.1.2

# test the *REMOTE* db connections into the vm's mariadb, with the username/password your grails app uses:
mysql --host=10.1.1.2 --user=joe --password=joePa88 -e "SELECT username FROM db1.user"
Warning: Using a password on the command line interface can be insecure.
+---------------+
| username      |
+---------------+
| ausgov        |
| Dean          |
| genelle.stone |
| glenys.gould  |
| Lisa.howdin   |
| martin.bohun  |
| matt.parry    |
| nathan.wall   |
| Pablo         |
| wendy.dalton  |
+---------------+
```

Please note that the above values (IPv4 addresses, DB username/password, etc.) have to be in sync and match those in `grails-app/conf/Config.groovy`, `grails-app/conf/DataSource.groovy`, etc. For example for the test on my laptop I adjusted the apps settings as follows:


`grails-app/conf/Config.groovy` (adjuset mail server port to 25 from 1025, my laptop already runs SMTP server on port 25)
```
environments {
    development {
        grails.logging.jul.usebridge = true
        grails.converters.default.pretty.print = true
        grails.serverURL = "http://localhost:8080/SubscriberPOC"
        logfile.location = "/tmp"
        australia.gov.au.url = "http://test.australia.gov.au"

        grails {
            mail {
                host = "localhost"
                port = 25
                from = ''
            }
        }

       // ...
    }
```

`grails-app/conf/DataSource.groovy` (the db settings were set to match the values used by the above vagrant and mariadb setup)
```
environments {

    development {
        dataSource {
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
            username = "joe"
            password = "joePa88"
            url = "jdbc:mysql://10.1.1.2/db1?zeroDateTimeBehavior=convertToNull&autoReconnect=true"
            dbCreate = "update"

            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 1
                initialSize = 1

                numTestsPerEvictionRun = 3
                maxWait = 10000

                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true

                validationQuery = "select now()"

                minEvictableIdleTimeMillis = 1000 * 60 * 5
                timeBetweenEvictionRunsMillis = 1000 * 60 * 5


                dbProperties {
                    autoReconnect = true
                }
            }

        }

        hibernate {
            cache.use_second_level_cache = true
            cache.use_query_cache = false
            cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
            singleSession = true // configure OSIV singleSession mode
            flush.mode = 'manual' // OSIV session flush mode outside of transactional context
        }
    }

    // ...

```

prod/test tomcat settings/configuration
```
[svc_fin_mrs_t_app@FmrsSrv01Cbr01 ~]$ cd apache-tomcat/bin
[svc_fin_mrs_t_app@FmrsSrv01Cbr01 bin]$ ./version.sh
Using CATALINA_BASE:   /app/mediareleases/apache-tomcat
Using CATALINA_HOME:   /app/mediareleases/apache-tomcat
Using CATALINA_TMPDIR: /app/mediareleases/apache-tomcat/temp
Using JRE_HOME:        /opt/jdk1.8.0_25
Using CLASSPATH:       /app/mediareleases/apache-tomcat/bin/bootstrap.jar:/app/mediareleases/apache-tomcat/bin/tomcat-juli.jar
Server version: Apache Tomcat/8.0.27
Server built:   Sep 28 2015 08:17:25 UTC
Server number:  8.0.27.0
OS Name:        Linux
OS Version:     3.10.0-229.11.1.el7.x86_64
Architecture:   amd64
JVM Version:    1.8.0_25-b17
JVM Vendor:     Oracle Corporation
```
```
[svc_fin_mrs_t_app@FmrsSrv01Cbr01 apache-tomcat]$ cat bin/setenv.sh
export JAVA_HOME=/opt/jdk1.8.0_25
CATALINA_OPTS="-server -noverify -Xshare:off -Xms512M -Xmx512M -XX:MaxPermSize=256M -XX:PermSize=128M -XX:+UseParallelGC"
CATALINA_OPTS="${CATALINA_OPTS} -Djava.net.preferIPv4Stack=true -XX:+EliminateLocks -XX:+UseBiasedLocking"
```

check/verify the tomcat installation/configuration
```
mbohun@linux-9gpe:~/Downloads/apache-tomcat-7.0.70/bin> ./version.sh
Using CATALINA_BASE:   /home/mbohun/Downloads/apache-tomcat-7.0.70
Using CATALINA_HOME:   /home/mbohun/Downloads/apache-tomcat-7.0.70
Using CATALINA_TMPDIR: /home/mbohun/Downloads/apache-tomcat-7.0.70/temp
Using JRE_HOME:        /usr/local/jdk/jre
Using CLASSPATH:       /home/mbohun/Downloads/apache-tomcat-7.0.70/bin/bootstrap.jar:/home/mbohun/Downloads/apache-tomcat-7.0.70/bin/tomcat-juli.jar
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on
Server version: Apache Tomcat/7.0.70
Server built:   Jun 15 2016 16:27:45 UTC
Server number:  7.0.70.0
OS Name:        Linux
OS Version:     4.6.2-1-default
Architecture:   amd64
JVM Version:    1.7.0_80-b15
JVM Vendor:     Oracle Corporation
```
