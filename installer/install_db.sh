#!/bin/bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

db_root_password="blah"

is_mysql_root_password_set() {
	! mysqladmin --user=root --password="" status > /dev/null 2>&1
}

# don't forget this script has to run in NON-INTERACTIVE mode!
sudo yum -y install mariadb-server

# TODO: check if ansible requires this (if yes, is it possible to install it with ansible)?
#       https://docs.ansible.com/ansible/mysql_db_module.html#notes
sudo yum -y install MySQL-python

# enable and start the db engine
sudo systemctl enable mariadb
sudo systemctl start mariadb

if is_mysql_root_password_set; then
	echo "Database root password already set"
else
	
	mysql --user=root --password="" <<EOF
  UPDATE mysql.user SET Password=PASSWORD('${db_root_password}') WHERE User='root';
  DELETE FROM mysql.user WHERE User='';
  DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
  DROP DATABASE IF EXISTS test;
  DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
  CREATE DATABASE IF NOT EXISTS db1;
  CREATE USER 'joe'@'10.1.1.%' IDENTIFIED BY 'joePa88';
  GRANT ALL PRIVILEGES ON db1.* TO 'joe'@'10.1.1.%';
  FLUSH PRIVILEGES;
EOF
	
fi

# TODO: create db1 if it does not exists
# mysql --user=root --password="${db_root_password}" -e "CREATE DATABASE IF NOT EXISTS db1"

# import the sql dump into the db
# mysql --user=root --password="${db_root_password}" db1 < /home/vagrant/sync/mrs_16Jun2016_prod.sql

# NOTE: use ssh port forwarding:
#       ssh -f vagrant@10.1.1.2 -L 3333:10.1.1.2:3306 -N
