package subscriberpoc

class Subscriber {

    String email
    String confirmCode
    boolean verified
    Boolean accepted
    Date dateCreated
    Date lastUpdated

    static hasMany = [topics: Topic]

    static constraints = {
        email email:true, unique: true
        confirmCode nullable: true
        accepted nullable: false
        topics nullable: false
    }
}
