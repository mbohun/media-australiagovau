package subscriberpoc

class User {

	transient springSecurityService

	String username
	String password
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    String email
    Date lastPwdChange
    String[] pwdHistory = ['one','two','three','four','five','six','seven','eight']



	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password nullable: true, validator: {passwd, user ->
			passwd != user.username
		}
        email email: true, unique: true
        lastPwdChange nullable: true
        pwdHistory nullable: true

	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
}


