package subscriberpoc

class Topic {

    String name
    String description

    static hasMany = [subscribers: Subscriber]
    static belongsTo= Subscriber

    static constraints = {
        name maxSize: 100, unique: true
        description nullable: true
    }

    String toString() {
        return name
    }
}
