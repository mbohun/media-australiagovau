package subscriberpoc

/*
 * To be created by reading metadata
 * when the cron job crawls agency url
 */
class MediaRelease {

    String url
    String shortUrl
    String title
    String snippet
    Date dateCreated
    boolean isMediaRelease
    boolean hasBeenSent = false
    boolean verified = false

    static belongsTo = [site: Site]

    static mapping = {
        autoTimestamp true
    }

    static constraints = {
        title blank: false
        url url: true, unique: true
        shortUrl url: true, nullable: true
        snippet maxSize:  1024, nullable: true
        dateCreated nullable: true



    }


}
