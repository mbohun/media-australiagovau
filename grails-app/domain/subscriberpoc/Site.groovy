package subscriberpoc
//@Resource(uri='/site')
class Site {

    String url
    String description
    String mediaReleaseSelector
    String releaseUrl

    static constraints = {
        agency blank: false
        url blank: false
        topics blank: false, nullable: false
        description nullable: true
        mediaReleaseSelector nullable: true
        releaseUrl nullable: false
    }

    static belongsTo = [agency: Agency]
    static hasMany = [releases: MediaRelease, topics: Topic]

    static mapping = {
        agency lazy: false
    }

    String toString(){
        return url
    }
}
