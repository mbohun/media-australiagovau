package subscriberpoc

class Agency {

    String title

    static hasMany = [sites: Site]

    static mapping = {
        sites lazy: true
        sort "title"
    }


    static constraints = {
        title blank: false, unique: true, maxSize: 100

    }

    String toString() {
        return title
    }
}
