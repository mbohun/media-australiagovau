import subscriberpoc.SubscriberJsonMarshaller
import subscriberpoc.SubscriberXmlMarshaller
import subscriberpoc.TopicJsonMarshaller
import subscriberpoc.TopicXmlMarshaller
import util.CustomObjectMarshallers
// Place your Spring DSL code here
beans = {


    customObjectMarshallers(CustomObjectMarshallers){
        marshallers = [
                new SubscriberJsonMarshaller() ,
                new SubscriberXmlMarshaller(),
                new TopicJsonMarshaller() ,
                new TopicXmlMarshaller()
        ]
    }

}
