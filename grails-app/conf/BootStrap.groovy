import org.springframework.web.context.support.WebApplicationContextUtils
import grails.util.Environment
import subscriberpoc.*

class BootStrap {

    Topic benefits
    Topic business
    Topic culture
    Topic education
    Topic enviro
    Topic family
    Topic health
    Topic immigration
    Topic itc
    Topic jobs
    Topic money
    Topic passports
    Topic law
    Topic security
    Topic transport
    Topic aboutgov
    Agency agriculture
    Agency ags
    Agency comms
    Agency defence
    Agency vet
    Agency edu
    Agency employment
    Agency env
    Agency finance
    Agency fa
    Agency heal
    Agency immi
    Agency industry
    Agency infra
    Agency pm
    Agency ss
    Agency dhs
    Agency treasury

    def grailsApplication

    def init = { servletContext ->

        // NOTE: report the Environment current type, because depending on the current Environment type
        //       MRS behavior/processing changes, for example in Environment type DEVELOPMENT and TEST
        //       MRS will remove ALL subscribers from the DB except those subscriber you specified
        //       in MRS_TEST_SUBSCRIBER_EMAILS
        //
        log.info("MRS is starting in Environment: " + Environment.current)

        // CrawlerJob cron configuration
        def crawlerJobCron = grailsApplication.config.crawlerJob.cron
        log.debug("subscriberpoc.CrawlerJob cron: " + crawlerJobCron)
        CrawlerJob.schedule(crawlerJobCron)

        def springContext = WebApplicationContextUtils.getWebApplicationContext( servletContext )
        // Custom marshalling
        springContext.getBean( "customObjectMarshallers" ).register()

        log.debug("Checking number of admin accounts: " + User.count())

        if (!User.count()) {
            log.warn("No admin accounts found, creating admin account...")

            final def mrsAdminUsername = System.getenv("MRS_ADMIN_USERNAME")
            final def mrsAdminPassword = System.getenv("MRS_ADMIN_PASSWORD")
            final def mrsAdminEmail    = System.getenv("MRS_ADMIN_EMAIL")

            try {
                // NOTE: check if we do have an adminRole (starting with populated/existing MRS DB), if not (starting with an empty MRS DB) then create one
                def adminRole = Role.findByAuthority('ROLE_ADMIN')
                log.debug("adminRole: " + adminRole)

                if (adminRole == null) {
                    log.debug("ROLE_ADMIN not found creating it...")
                    adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
                    log.debug("adminRole: " + adminRole)
                }

                final def admin = new User(username:        mrsAdminUsername,
                                           password:        mrsAdminPassword,
                                           email:           mrsAdminEmail,
                                           passwordExpired: false,
                                           enabled:         true,
                                           pwdHistory:      ['','','','','','','','']).save()

                UserRole.create admin, adminRole, true
                log.debug("Created admin account for: " + mrsAdminUsername)

            } catch (Exception e) {
                log.error("Creation of admin account FAILED: " + e.getMessage())
                e.printStackTrace()
            }

            // TODO: do we want to send an email to the newly created admin?
        }

        if(!Agency.count()) {
            log.debug("Creating Data")
            createProductionTopics()
            createProductionAgencies()
            createProductionData()
            log.debug("Done Creating Data")
        }

        // NOTE: IF starting MRS:
        //       - in dev or test Environment, AND
        //       - with the DB populated with real data (prod SQL dump)
        //       THEN DELETE *ALL* (the real) subscribers,
        //       EXCEPT those with emails listed in MRS_TEST_SUBSCRIBER_EMAILS.
        //       Those listed in MRS_TEST_SUBSCRIBER_EMAILS should normally be the MRS' developers, testers, etc.
        //
        if ((Environment.getCurrent() == Environment.DEVELOPMENT) || (Environment.getCurrent() == Environment.TEST)) {
            def final MRS_TEST_SUBSCRIBER_EMAILS = (System.getenv("MRS_TEST_SUBSCRIBER_EMAILS")?: '').tokenize(" ")
            log.info("MRS_TEST_SUBSCRIBER_EMAILS set to: " + MRS_TEST_SUBSCRIBER_EMAILS)
            log.info("MRS is starting in Environment: " + Environment.current
                     + " and therefore DELETING all subscribers (except those listed in MRS_TEST_SUBSCRIBER_EMAILS: " + MRS_TEST_SUBSCRIBER_EMAILS + ")")

            log.info("found: " + Subscriber.count() + " subscribers")

            Subscriber.findAllByEmailNotInList(MRS_TEST_SUBSCRIBER_EMAILS)*.delete(flush: true)
            log.info("after delete: " + Subscriber.count() + " subscribers")

            for (Subscriber subscriber : Subscriber.list(sort: "email")) {
                log.debug("preserved subscriber: " + subscriber.email)
            }
        }
    }
    def destroy = {
    }




    private createProductionTopics() {
        log.debug("Creating Topics")
        // These are based on the topics from australia.gov.au
        benefits = new Topic(name: 'Benefits and Payments', description: 'Aged care, carers, crisis, family, indigenous, jobseekers, veterans, youth payments and services').save()
        business = new Topic(name: 'Business and Industry', description: 'ABN, grants, non-profit and small business, primary industry, import and export, science and technology, tenders').save()
        culture = new Topic(name: 'Culture and Arts', description: 'Indigenous heritage and history, arts grants, cultural institutions, heritage, history, honours and awards, family history').save()
        education = new Topic(name: 'Education and Training', description: 'Early childhood, school, higher education, international students, skills recognition, scholarships, VET').save()
        enviro = new Topic(name: 'Environment', description: 'Energy efficiency, environmental management and protection, biodiversity, grants, natural resources').save()
        family = new Topic(name: 'Family and Community', description: 'Births, deaths and marriages, child care, housing and property, relationships, social issues').save()
        health = new Topic(name: "Health", description: "Workplace health and safety, drug and alcohol use, health promotion, childrens\'s health, mental health,sport").save()
        immigration = new Topic(name: 'Immigration and Visas', description: 'Australian citizenship, customs, work, study and short-term visas, migration and tourism').save()
        itc = new Topic(name: 'IT and Communications', description: 'Data, internet, postal services, television and radio').save()
        jobs = new Topic(name: 'Jobs and Workplace', description: 'Careers, government jobs, employment services, working conditions').save()
        money = new Topic(name: 'Money and Tax', description: 'Tax returns, ABN, superannuation, personal finance, financial regulation').save()
        passports = new Topic(name: 'Passports and Travel', description: 'Customs and quarantine, embassies and consulates, travelling overseas').save()
        law = new Topic(name: 'Public Safety and Law', description: 'Consumer protection, online safety, emergency services, legislation, police, rights').save()
        security = new Topic(name: 'Security and Defence', description: 'National security, cyber security, ADF, military history, commemoration').save()
        transport = new Topic(name: 'Transport and Regional', description: 'Registration and licences, roads and transport, aviation, regional development').save()
        aboutgov = new Topic(name: 'About Government', description: 'Government services, parliament, international relations, Indigenous affairs, elections, matters of state').save()
        log.debug("Done Creating Topics")
    }

    private createProductionAgencies() {
        log.debug("Creating Agencies")
        agriculture = new Agency(title: "Dept of Agriculture").save()
        ags = new Agency(title: "Attorney Generals").save()
        comms = new Agency(title: "Dept of Communications").save()
        defence = new Agency(title: "Dept of Defence").save()
        vet = new Agency(title: "Dept of Veterans Affairs").save()
        edu = new Agency(title: "Dept of Education").save()
        employment = new Agency(title: "Dept of Employment").save()
        env = new Agency(title: "Dept of Environment").save()
        finance = new Agency(title: "Dept of Finance").save()
        fa = new Agency(title: "Dept of Foreign Affairs").save()
        heal = new Agency(title: "Dept of Health").save()
        immi = new Agency(title: "Dept of Immigration and Border").save()
        industry = new Agency(title: "Dept of Industry").save()
        infra = new Agency(title: "Dept of Infrastructure").save()
        pm = new Agency(title: "Prime Minister and Cabinet").save()
        ss = new Agency(title: "Dept of Social Services").save()
        dhs = new Agency(title: "Dept of Human Services").save()
        treasury = new Agency(title: "Dept of Treasury").save()
        log.debug("Done Creating Agencies")
    }

    private createProductionData(){
        log.debug("Creating Sample ProdData")

        Subscriber oneSub = new Subscriber(email: "mediareleasetester@gmail.com", verified: true, confirmCode: "a90ab7d7-31ef-41b0-b814-bee216bc1436", topics: [Topic.findById(1), Topic.findById(10), Topic.findById(3)]).save(flush: true)
        Subscriber twoSub = new Subscriber(email: "media.releasetester@gmail.com", verified: false, confirmCode: "blah", topics: [Topic.findById(2)]).save(flush: true)
        Subscriber threeSub = new Subscriber(email: "thisisjustatest@gmail.com", verified: true, confirmCode: "whateva-31ef-41b0-b814-bee216bc1436", topics: [Topic.findById(1), Topic.findById(10), Topic.findById(8)]).save(flush: true)

        Site site;


        //Dept of Agriculture
        site = new Site(agency: Agency.findByTitle("Dept of Agriculture"), topics: [Topic.findByName("Business and Industry"), Topic.findByName("Passports and Travel")],url: "http://www.agricultureminister.gov.au/Pages/Media-Releases.aspx", releaseUrl: "/Pages/Media-Releases/").save()
        agriculture.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Agriculture"), topics: [Topic.findByName("Business and Industry"), Topic.findByName("Passports and Travel")],url: "http://www.agricultureparlsec.gov.au/Pages/default.aspx", releaseUrl: "/Pages/Media-Releases/").save()
        agriculture.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Agriculture"), topics: [Topic.findByName("Business and Industry"), Topic.findByName("Passports and Travel")],url: "http://www.agriculture.gov.au/about/media-centre/dept-releases/2015", releaseUrl: "/about/media-centre/dept-releases/2015/").save()
        agriculture.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Agriculture"), topics: [Topic.findByName("Business and Industry")],url: "http://www.grdc.com.au/Media-Centre/Media-News", releaseUrl: "/media-centre/media-news/").save()
        agriculture.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Agriculture"), topics: [Topic.findByName("Business and Industry")],url: "http://www.rirdc.gov.au/news", releaseUrl: "/news/2015/").save()
        agriculture.addToSites(site).save()


        //Attorney Generals
        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Benefits and Payments"), Topic.findByName("Public Safety and Law"), Topic.findByName("Culture and Arts")],url: "http://www.attorneygeneral.gov.au/mediareleases/Pages/default.aspx", releaseUrl: "/Mediareleases/Pages/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law"), Topic.findByName("Culture and Arts")],url: "http://www.ministerjustice.gov.au/Mediareleases/Pages/default.aspx", releaseUrl: "/Mediareleases/Pages/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "https://www.crimecommission.gov.au/media-centre/media-releases-and-statements", releaseUrl: "/media/20").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "http://www.afp.gov.au/media-centre/news.aspx", releaseUrl: "/media-centre/news/afp/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "http://www.aic.gov.au/media/2015.html", releaseUrl: "/media/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "http://www.alrc.gov.au/news-media", releaseUrl: "/news-media/media-release/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "http://www.cdpp.gov.au/Media/Releases/", releaseUrl: "/news/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "http://www.humanrights.gov.au/news/media-releases", releaseUrl: "/news/media-releases/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "http://www.oaic.gov.au/news-and-events/media-releases/", releaseUrl: "/news-and-events/media-releases/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://www.australiacouncil.gov.au/news/media-centre/media-releases/", releaseUrl: "/news/media-centre/media-releases/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Public Safety and Law")],url: "http://austrac.gov.au/media/media-releases", releaseUrl: "/media/media-releases/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://www.anmm.gov.au/About-Us/Media", releaseUrl: "/About-Us/Media/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://www.naa.gov.au/about-us/media/media-releases/index.aspx", releaseUrl: "/about-us/media/media-releases/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://www.nfsa.gov.au/about/media/releases/", releaseUrl: "/about/media/releases/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://www.nla.gov.au/news/recent-media-releases", releaseUrl: "/media-releases/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://www.nma.gov.au/media/media_releases_index", releaseUrl: "/media/media_releases_by_year/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://www.screenaustralia.gov.au/news_and_events/Media-Releases.aspx", releaseUrl: "/news_and_events/2015/").save()
        ags.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Attorney Generals"), topics: [Topic.findByName("Culture and Arts")],url: "http://arts.gov.au/news", releaseUrl: "/news/2015/").save()
        ags.addToSites(site).save()


        //Dept of Communications
        site = new Site(agency: Agency.findByTitle("Dept of Communications"), topics: [Topic.findByName("IT and Communications")],url: "http://www.minister.communications.gov.au/malcolm_turnbull/news", releaseUrl: "/malcolm_turnbull/news/").save()
        comms.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Communications"), topics: [Topic.findByName("IT and Communications")],url: "http://www.minister.communications.gov.au/paul_fletcher/news", releaseUrl: "/paul_fletcher/news/").save()
        comms.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Communications"), topics: [Topic.findByName("IT and Communications")],url: "https://www.communications.gov.au/departmental-news", releaseUrl: "/departmental-news/").save()
        comms.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Communications"), topics: [Topic.findByName("IT and Communications")],url: "http://about.abc.net.au/press-releases/", releaseUrl: "/press-releases/").save()
        comms.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Communications"), topics: [Topic.findByName("IT and Communications")],url: "https://auspost.newsroom.com.au/", releaseUrl: "/Content/Home/02-Home/Article/").save()
        comms.addToSites(site).save()


        //Dept of Defence
        site = new Site(agency: Agency.findByTitle("Dept of Defence"), topics: [Topic.findByName("Security and Defence")],url: "http://news.defence.gov.au/feed/", releaseUrl: "/2015/").save()
        defence.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Defence"), topics: [Topic.findByName("Benefits and Payments"), Topic.findByName("Culture and Arts"), Topic.findByName("Security and Defence")],url: "http://www.minister.defence.gov.au/kevin-andrews-media-releases/", releaseUrl: "/2015/").save()
        defence.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Defence"), topics: [Topic.findByName("Benefits and Payments"), Topic.findByName("Culture and Arts"), Topic.findByName("Security and Defence")],url: "http://www.minister.defence.gov.au/robert-media-release-archive/", releaseUrl: "/2015/").save()
        defence.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Defence"), topics: [Topic.findByName("Benefits and Payments"), Topic.findByName("Culture and Arts"), Topic.findByName("Security and Defence")],url: "http://www.minister.defence.gov.au/chester-media-release-archive/", releaseUrl: "/2015/").save()
        defence.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Defence"), topics: [Topic.findByName("Money and Tax")],url: "http://www.dha.gov.au/about-us/news-and-events/news", releaseUrl: "/about-us/news-and-events/news/").save()
        defence.addToSites(site).save()


        //Veterans Affairs
        site = new Site(agency: Agency.findByTitle("Dept of Veterans Affairs"), topics: [Topic.findByName("Benefits and Payments"), Topic.findByName("Culture and Arts")],url: "http://minister.dva.gov.au/media_releases.htm", releaseUrl: "/media_releases/2015/").save()
        vet.addToSites(site).save()


        //Dept of Education
        site = new Site(agency: Agency.findByTitle("Dept of Education"), topics: [Topic.findByName("Education and Training"), Topic.findByName("Immigration and Visas")],url: "http://education.gov.au/news", releaseUrl: "/news/").save()
        edu.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Education"), topics: [Topic.findByName("Education and Training"), Topic.findByName("Immigration and Visas")],url: "http://ministers.education.gov.au/media-release", releaseUrl: "/pyne/").save()
        edu.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Education"), topics: [Topic.findByName("Education and Training"), Topic.findByName("Immigration and Visas")],url: "http://ministers.education.gov.au/media-release?foo=bar", releaseUrl: "/ryan/").save()
        edu.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Education"), topics: [Topic.findByName("Education and Training"), Topic.findByName("Immigration and Visas")],url: "http://ministers.education.gov.au/media-release?foo=again", releaseUrl: "/birmingham/").save()
        edu.addToSites(site).save()


        //Dept of Employment
        site = new Site(agency: Agency.findByTitle("Dept of Employment"), topics: [Topic.findByName("Jobs and Workplace")],url: "http://ministers.employment.gov.au/hartsuyker", releaseUrl: "/hartsuyker/").save()
        employment.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Employment"), topics: [Topic.findByName("Jobs and Workplace")],url: "http://ministers.employment.gov.au/abetz", releaseUrl: "/abetz/").save()
        employment.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Employment"), topics: [Topic.findByName("Jobs and Workplace")],url: "http://comcare.gov.au/news__and__media/media_centre", releaseUrl: "/news__and__media/media_centre/").save()
        employment.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Employment"), topics: [Topic.findByName("Jobs and Workplace")],url: "http://www.safeworkaustralia.gov.au/sites/swa/media-events/media-releases/pages/media-releases", releaseUrl: "/sites/swa/media-events/media-releases/pages/mr").save()
        employment.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Employment"), topics: [Topic.findByName("Jobs and Workplace")],url: "https://www.wgea.gov.au/news-and-media/media-releases", releaseUrl: "/media-releases/").save()
        employment.addToSites(site).save()


        //Dept of Environment
        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.environment.gov.au/mediarelease/department-media-releases", releaseUrl: "/mediarelease/").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.environment.gov.au/minister/hunt/2015/index.html", releaseUrl: "/minister/hunt/2015/mr").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.environment.gov.au/minister/baldwin/2015/index.html", releaseUrl: "/minister/baldwin/2015/mr").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.antarctica.gov.au/news", releaseUrl: "/news/2015/").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://media.bom.gov.au/releases/", releaseUrl: "/releases/").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.cleanenergyregulator.gov.au/About/News-and-updates", releaseUrl: "/About/Pages/News").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.gbrmpa.gov.au/media-room/latest-news", releaseUrl: "/media-room/latest-news/").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.mdba.gov.au/media-pubs/mr", releaseUrl: "/media-pubs/mr/").save()
        env.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Environment"), topics: [Topic.findByName("Environment")],url: "http://www.harbourtrust.gov.au/media-releases", releaseUrl: "/mediarelease/").save()
        env.addToSites(site).save()


        //Dept of Finance
        site = new Site(agency: Agency.findByTitle("Dept of Finance"), topics: [Topic.findByName("Business and Industry")],url: "http://www.financeminister.gov.au/media/2015/", releaseUrl: "/media/2015/").save()
        finance.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Finance"), topics: [Topic.findByName("About Government")],url: "http://www.smos.gov.au/media/2015/", releaseUrl: "/media/2015/").save()
        finance.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Finance"), topics: [Topic.findByName("Money and Tax")],url: "http://www.parlsecfinance.gov.au/media/2015", releaseUrl: "/media/2015/").save()
        finance.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Finance"), topics: [Topic.findByName("About Government")],url: "http://www.aec.gov.au/media/media-releases/", releaseUrl: "/media/media-releases/2015").save()
        finance.addToSites(site).save()


        //Dept of Foreign Affairs
        site = new Site(agency: Agency.findByTitle("Dept of Foreign Affairs"), topics: [Topic.findByName("Passport and Travel")],url: "http://foreignminister.gov.au/releases/Pages/default.aspx", releaseUrl: "/releases/Pages/2015/jb_mr_").save()
        fa.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Foreign Affairs"), topics: [Topic.findByName("Business and Industry")],url: "http://trademinister.gov.au/releases/Pages/default.aspx", releaseUrl: "/releases/Pages/2015/ar").save()
        fa.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Foreign Affairs"), topics: [Topic.findByName("Passport and Travel")],url: "http://ministers.dfat.gov.au/ciobo/releases/Pages/default.aspx", releaseUrl: "/ciobo/releases/Pages/2015/sc").save()
        fa.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Foreign Affairs"), topics: [Topic.findByName("Passport and Travel"), Topic.findByName("Business and Industry")],url: "http://dfat.gov.au/news/media-releases/Pages/departmental-media-releases.aspx", releaseUrl: "/news/media-releases/Pages/").save()
        fa.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Foreign Affairs"), topics: [Topic.findByName("Business and Industry")],url: "http://www.austrade.gov.au/News/Media-Releases/2015", releaseUrl: "/about-austrade/news/media-releases/2015/").save()
        fa.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Foreign Affairs"), topics: [Topic.findByName("Business and Industry")],url: "http://www.efic.gov.au/news-events/media-releases/", releaseUrl: "/news-events/media-releases/2015/").save()
        fa.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Foreign Affairs"), topics: [Topic.findByName("Passport and Travel"), Topic.findByName("Immigration and Visas")],url: "http://www.tourism.australia.com/Media-Releases.aspx", releaseUrl: "/news/Media-Releases").save()
        fa.addToSites(site).save()


        //Dept of Health
        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.health.gov.au/internet/ministers/publishing.nsf/Content/the-hon-sussan-ley-mp-latest-news", releaseUrl: "/internet/ministers/publishing.nsf/Content/health-mediarel-yr2015-ley").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.health.gov.au/internet/ministers/publishing.nsf/Content/the-hon-fiona-nash-latest-news", releaseUrl: "/internet/ministers/publishing.nsf/Content/health-mediarel-yr2015-nash").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.health.gov.au/internet/main/publishing.nsf/Content/Departmental+Media+Releases-1", releaseUrl: "/internet/main/publishing.nsf/Content/mr-yr15-dept").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.aihw.gov.au/media-releases/", releaseUrl: "/media-release-detail/?id=").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.arpansa.gov.au/News/MediaReleases/index.cfm", releaseUrl: "/News/MediaReleases/mr").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.foodstandards.gov.au/media/pages/mediareleases/Default.aspx", releaseUrl: "/media/Pages/").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.healthdirect.gov.au/media-releases", releaseUrl: "-").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.nhmrc.gov.au/media/releases/2015", releaseUrl: "/media/releases/2015/").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.phio.org.au/publications/publications/media-releases.aspx", releaseUrl: "/downloads/file/PublicationItems/MediaRelease").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.tga.gov.au/media-releases-statements", releaseUrl: "/alert/").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "http://www.ausport.gov.au/news/ais_news", releaseUrl: "/news/ais_news/story").save()
        heal.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Health"), topics: [Topic.findByName("Family and Community")],url: "https://www.asada.gov.au/news-type/media-statements", releaseUrl: "/news/").save()
        heal.addToSites(site).save()


        //Dept of Immigration and Border Protection
        site = new Site(agency: Agency.findByTitle("Dept of Immigration and Border"), topics: [Topic.findByName("Immigration and Visas")],url: "http://www.border.gov.au", releaseUrl: "http://www.border.gov.au").save()
        immi.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Immigration and Border"), topics: [Topic.findByName("Immigration and Visas")],url: "http://www.minister.border.gov.au/peterdutton/Pages/Welcome.aspx", releaseUrl: "/peterdutton/2015/pages/").save()
        immi.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Immigration and Border"), topics: [Topic.findByName("Immigration and Visas")],url: "http://www.minister.border.gov.au/michaeliacash/Pages/Welcome.aspx", releaseUrl: "/michaeliacash/2015/pages/").save()
        immi.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Immigration and Border"), topics: [Topic.findByName("Immigration and Visas"), Topic.findByName("Passport and Travel")],url: "http://newsroom.border.gov.au/", releaseUrl: "/releases/").save()
        immi.addToSites(site).save()


        //Dept of Industry
        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://minister.industry.gov.au/ministers/macfarlane", releaseUrl: "/ministers/macfarlane/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://minister.industry.gov.au/ministers/andrews", releaseUrl: "/ministers/andrews/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://asbc.gov.au/news/media-releases", releaseUrl: "/news/media-release/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://www.aims.gov.au/docs/media/latest-releases", releaseUrl: "/docs/media/latest-releases/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://www.ansto.gov.au/AboutANSTO/MediaCentre/News/", releaseUrl: "/AboutANSTO/MediaCentre/News/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://www.arc.gov.au/news-media/media-releases", releaseUrl: "/news-media/media-releases/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://arena.gov.au/news-media/media-releases/", releaseUrl: "/media/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://www.csiro.au/en/News/News-releases", releaseUrl: "/en/News/News-releases/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://www.ga.gov.au/news-events/news", releaseUrl: "/news-events/news/latest-news/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry")],url: "http://www.ipaustralia.gov.au/about-us/news-media-and-events/latest-news-listing/", releaseUrl: "/about-us/news-media-and-events/latest-news-listing/").save()
        industry.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Industry"), topics: [Topic.findByName("Business and Industry"), Topic.findByName("Culture and Arts")],url: "http://www.questacon.edu.au/business/media-centre/media/", releaseUrl: "/business/media-centre/news-and-media/").save()
        industry.addToSites(site).save()


        //Dept of Industry
        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "https://infrastructure.gov.au/department/media/releases_speeches.aspx/", releaseUrl: "/media/mr_").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "http://minister.infrastructure.gov.au/wt/", releaseUrl: "/wt/releases/2015").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "http://www.minister.infrastructure.gov.au/jb/releases/2015/", releaseUrl: "/jb/releases/2015/").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "http://newsroom.airservicesaustralia.com/releases", releaseUrl: "/releases/").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "http://www.amsa.gov.au/media/media-releases/2015/", releaseUrl: "/media/media-releases/2015/documents/").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "http://atsb.gov.au/newsroom/media.aspx", releaseUrl: "/newsroom/2015/").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "https://www.casa.gov.au/about-casa/standard-page/media-releases", releaseUrl: "/about-casa/standard-page/media-releases").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "http://www.ntc.gov.au/about-ntc/news/media-releases/", releaseUrl: "/about-ntc/news/media-releases/").save()
        infra.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Infrastructure"), topics: [Topic.findByName("Transport and Regional")],url: "http://www.nationalcapital.gov.au/index.php/media-centre/2015-media-releases", releaseUrl: "/index.php/media-centre/").save()
        infra.addToSites(site).save()


        //PM&C
        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("About Government")],url: "http://www.pm.gov.au/media", releaseUrl: "/media/").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("About Government")],url: "http://www.gg.gov.au/media-releases", releaseUrl: "/media-release/").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("About Government")],url: "http://www.apsc.gov.au/publications-and-media/media-releases", releaseUrl: "/publications-and-media/media-releases/").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("About Government")],url: "http://minister.indigenous.gov.au/media", releaseUrl: "/media/2015").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("About Government")],url: "http://minister.women.gov.au/media", releaseUrl: "/media/2015").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("About Government")],url: "http://christianporter.dpmc.gov.au/media", releaseUrl: "/media/2015").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("About Government")],url: "http://alantudge.dpmc.gov.au/media", releaseUrl: "/media/2015").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("Environment")],url: "http://www.nntt.gov.au/News-and-Publications/Pages/default.aspx", releaseUrl: "/News-and-Publications/latest-news/Pages/").save()
        pm.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Prime Minister and Cabinet"), topics: [Topic.findByName("Public Safety and Law")],url: "http://www.ombudsman.gov.au/media-releases/", releaseUrl: "/media-releases/show/").save()
        pm.addToSites(site).save()


        //Social Services
        site = new Site(agency: Agency.findByTitle("Dept of Social Services"), topics: [Topic.findByName("Benefits and Payments"), Topic.findByName("Family and Community")],url: "http://scottmorrison.dss.gov.au/media-releases", releaseUrl: "/media-releases/").save()
        ss.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Social Services"), topics: [Topic.findByName("Benefits and Payments")],url: "http://mitchfifield.dss.gov.au/media-releases", releaseUrl: "/media-releases/").save()
        ss.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Social Services"), topics: [Topic.findByName("Benefits and Payments")],url: "http://concettafierravantiwells.dss.gov.au/media-releases", releaseUrl: "/media-releases/").save()
        ss.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Social Services"), topics: [Topic.findByName("Benefits and Payments")],url: "https://www.dss.gov.au/about-the-department/news-events", releaseUrl: "/news/2015/").save()
        ss.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Social Services"), topics: [Topic.findByName("Family and Community")],url: "https://aifs.gov.au/media-centre", releaseUrl: "/media-releases/").save()
        ss.addToSites(site).save()


        //Department of Human Services
        site = new Site(agency: Agency.findByTitle("Dept of Human Services"), topics: [Topic.findByName("Family and Community"), Topic.findByName("Benefits and Payments"), Topic.findByName("Business and Industry")],url: "http://www.humanservices.gov.au/corporate/media/media-releases/2015/index", releaseUrl: "/corporate/media/media-releases/2015/").save()
        dhs.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Human Services"), topics: [Topic.findByName("Family and Community"), Topic.findByName("Benefits and Payments")],url: "http://www.mhs.gov.au/media/media_releases/index", releaseUrl: "2015/").save()
        dhs.addToSites(site).save()


        //Treasury
        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax"), Topic.findByName("Business and Industry")],url: "http://jbh.ministers.treasury.gov.au/media-release/2015/", releaseUrl: "/media-release/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax"), Topic.findByName("Business and Industry")],url: "http://bfb.ministers.treasury.gov.au/media-release/2015/", releaseUrl: "/media-release/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax"), Topic.findByName("Business and Industry")],url: "http://jaf.ministers.treasury.gov.au/media-release/2015/", releaseUrl: "/media-release/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://kmo.ministers.treasury.gov.au/media-release/2015/", releaseUrl: "/media-release/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax"), Topic.findByName("Business and Industry")],url: "http://www.treasury.gov.au/PublicationsAndMedia/MediaReleases", releaseUrl: "/PublicationsAndMedia/MediaReleases/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://www.abs.gov.au/AUSSTATS/abs@.nsf/ViewContent?readform&view=mediareleasesbyReleaseDate&Action=Expand&Num=1.1", releaseUrl: "/AUSSTATS/abs@.nsf/mediareleasesbyReleaseDate/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax"), Topic.findByName("Business and Industry")],url: "http://accc.gov.au/media/media-releases", releaseUrl: "/media-release/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://www.apra.gov.au/MediaReleases/Pages/mediareleases.aspx?ys=2015&ye=2015&pg=1", releaseUrl: "/MediaReleases/Pages/15").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://www.asic.gov.au/about-asic/media-centre/find-a-media-release/?filter=2015", releaseUrl: "/about-asic/media-centre/find-a-media-release/2015-releases/15").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax"), Topic.findByName("Business and Industry")],url: "http://www.ato.gov.au/Media-centre/?sorttype=SortByType", releaseUrl: "/Media-centre/Media-release/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://www.pc.gov.au/news-media/media-releases", releaseUrl: "/news-media/media-releases/2").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://www.rba.gov.au/media-releases/index.html", releaseUrl: "/media-releases/2015/mr-").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://www.ramint.gov.au/media/press-releases/", releaseUrl: "/media/press-releases/2015/").save()
        treasury.addToSites(site).save()

        site = new Site(agency: Agency.findByTitle("Dept of Treasury"), topics: [Topic.findByName("Money and Tax")],url: "http://www.takeovers.gov.au/content/ListDocuments.aspx?Doctype=MR", releaseUrl: "DisplayDoc.aspx").save()
        treasury.addToSites(site).save()

    }


}
