import groovy.json.JsonSlurper

def slurper = new groovy.json.JsonSlurper()

environments {
    development {
        dataSource {
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
            username = "joe"
            password = "joePa88"
            url = "jdbc:mysql://10.1.1.2/db1?zeroDateTimeBehavior=convertToNull&autoReconnect=true"
            dbCreate = "update"

            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 1
                initialSize = 1

                numTestsPerEvictionRun = 3
                maxWait = 10000

                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true

                validationQuery = "select now()"

                minEvictableIdleTimeMillis = 1000 * 60 * 5
                timeBetweenEvictionRunsMillis = 1000 * 60 * 5


                dbProperties {
                    autoReconnect = true
                }
            }

        }

        hibernate {
            cache.use_second_level_cache = true
            cache.use_query_cache = false
//    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
            cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
            singleSession = true // configure OSIV singleSession mode
            flush.mode = 'manual' // OSIV session flush mode outside of transactional context
        }
    }
    sandbox {
        dataSource {
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
            username = "sandbox"
            password = "infoaccess"
            url = "jdbc:mysql://localhost/mediarelease?zeroDateTimeBehavior=convertToNull&autoReconnect=true"
            dbCreate = "update"

            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 1
                initialSize = 1

                numTestsPerEvictionRun = 3
                maxWait = 10000

                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true

                validationQuery = "select now()"

                minEvictableIdleTimeMillis = 1000 * 60 * 5
                timeBetweenEvictionRunsMillis = 1000 * 60 * 5


                dbProperties {
                    autoReconnect = true
                }
            }

        }

        hibernate {
            cache.use_second_level_cache = true
            cache.use_query_cache = true
            cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
        }
    }

    test {
        def CF_CONFIG_NOT_SET = '{ "DTO-staging-RDS-mysql": [ { "credentials": { "host": "not set", "jdbcUrl": "not set", "name": "not set", "password": "not set", "port": "not set", "uri": "not set", "username": "not set" } } ] }'
        def cf_config = slurper.parseText(System.getenv("VCAP_SERVICES")?: CF_CONFIG_NOT_SET)

        dataSource {
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
            username = cf_config["DTO-staging-RDS-mysql"][0]["credentials"]["username"]
            password = cf_config["DTO-staging-RDS-mysql"][0]["credentials"]["password"]
            url      = cf_config["DTO-staging-RDS-mysql"][0]["credentials"]["jdbcUrl"]
            dbCreate = "update"

            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 1
                initialSize = 1

                numTestsPerEvictionRun = 3
                maxWait = 10000

                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true

                validationQuery = "select now()"

                minEvictableIdleTimeMillis = 1000 * 60 * 5
                timeBetweenEvictionRunsMillis = 1000 * 60 * 5


                dbProperties {
                    autoReconnect = true
                }
            }

        }

        hibernate {
            cache.use_second_level_cache = true
            cache.use_query_cache = false
            cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
            singleSession = true // configure OSIV singleSession mode
            flush.mode = 'manual' // OSIV session flush mode outside of transactional context
        }
    }

    cloud {
        dataSource {
            pooled = true
            jmxExport = true

            driverClassName = "com.mysql.jdbc.Driver"
            dialect = org.hibernate.dialect.MySQL5InnoDBDialect
            uri = new URI(System.env.DATABASE_URL ?: "mysql://foo:bar@localhost")
            username = uri.userInfo ? uri.userInfo.split(":")[0] : ""
            password = uri.userInfo ? uri.userInfo.split(":")[1] : ""
            url = "jdbc:mysql://" + uri.host + uri.path
            dbCreate = "create-drop"

            properties {
                dbProperties {
                    autoReconnect = true
                }
            }
        }
        hibernate {
            cache.use_second_level_cache = true
            cache.use_query_cache = false
//    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
            cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
            singleSession = true // configure OSIV singleSession mode
            flush.mode = 'manual' // OSIV session flush mode outside of transactional context
        }
    }

    production {
        def CF_CONFIG_NOT_SET = '{ "DTO-prod-RDS-mysql": [ { "credentials": { "host": "not set", "jdbcUrl": "not set", "name": "not set", "password": "not set", "port": "not set", "uri": "not set", "username": "not set" } } ] }'
        def cf_config = slurper.parseText(System.getenv("VCAP_SERVICES")?: CF_CONFIG_NOT_SET)

        dataSource {
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
            username = cf_config["DTO-prod-RDS-mysql"][0]["credentials"]["username"]
            password = cf_config["DTO-prod-RDS-mysql"][0]["credentials"]["password"]
            url      = cf_config["DTO-prod-RDS-mysql"][0]["credentials"]["jdbcUrl"]
            dbCreate = "update"

            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 1
                initialSize = 1

                numTestsPerEvictionRun = 3
                maxWait = 10000

                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true

                validationQuery = "select now()"

                minEvictableIdleTimeMillis = 1000 * 60 * 5
                timeBetweenEvictionRunsMillis = 1000 * 60 * 5


                dbProperties {
                    autoReconnect = true
                }
            }

        }

        hibernate {
            cache.use_second_level_cache = true
            cache.use_query_cache = false
            cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
            singleSession = true // configure OSIV singleSession mode
            flush.mode = 'manual' // OSIV session flush mode outside of transactional context
        }
    }
}

