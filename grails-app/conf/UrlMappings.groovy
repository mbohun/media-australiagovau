class UrlMappings {

        static mappings = {
                "/$controller/$action?/$id?(.$format)?"{
                        constraints {
                                // apply constraints here
                        }
                }

                "/"(controller:"subscriber", action: "index")
                "/user/cancel"(controller:"user", action: "index")
                "500"(view:'/error')
                "404"(view:'/404')
                "/api/sites"(resources: 'siteRest')
                "/api/mediareleases"(resources: 'mediaReleaseRest')
                "/api/subscribers"(resources: 'subscriberRest')
                "/api/topics"(resources: 'topicRest')
                "/api/agencies"(resources: 'agencyRest')
                "/agency"(resources: 'agency')
                "/site"(resources: 'site')


        }
}
