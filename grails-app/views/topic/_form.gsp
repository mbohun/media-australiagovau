<%@ page import="subscriberpoc.Topic" %>


<div class="row">
	<div class="fieldcontain ${hasErrors(bean: topicInstance, field: 'name', 'error')} required">
		<div class="col-md-1">
			<label for="name">
				<g:message code="topic.name.label" default="Name" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-11">
			<g:textField name="name" maxlength="100" size="100" required="" value="${topicInstance?.name}"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: topicInstance, field: 'description', 'error')} ">
		<div class="col-md-1">
			<label for="description">
				<g:message code="topic.description.label" default="Description" />
			</label>
		</div>
		<div class="col-md-11">
			<g:textField name="description" size="100" value="${topicInstance?.description}"/>
		</div>
	</div>
</div>

<%--<div class="fieldcontain ${hasErrors(bean: topicInstance, field: 'subscribers', 'error')} ">
	<label for="subscribers">
		<g:message code="topic.subscribers.label" default="Subscribers" />
	</label>
</div>--%>

