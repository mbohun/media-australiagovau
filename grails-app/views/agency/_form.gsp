<%@ page import="subscriberpoc.Agency" %>



<div class="fieldcontain ${hasErrors(bean: agencyInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="agency.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" maxlength="100" required="" value="${agencyInstance?.title}"/>

</div>

