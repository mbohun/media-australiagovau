<%@ page import="subscriberpoc.Subscriber" %>
<g:set var="ausgovUrl" value="${grailsApplication.config.australia.gov.au.url}" />
<body>
<section>
	<div class="container">
		<div class="row">
			<div class="form-group fieldcontain ${hasErrors(bean: subscriberInstance, field: 'email', 'error')} required">
				<label for="email">
					<g:message code="subscriber.email.label" default="Email:"/><span class="required-indicator">*</span>
				</label>
				<g:if test="${editing != null && editing == true}">
					${subscriberInstance?.email}
				</g:if>
				<g:else>
					<g:field type="email" name="email" required="required" value="${subscriberInstance?.email}" size="50" />
				</g:else>
			</div>
		</div>
		<div class="row">
			<div class="form-group fieldcontain ${hasErrors(bean: subscriberInstance, field: 'dateCreated', 'error')} required">
				<label for="email">
					<g:message code="subscriber.dateCreated.label" default="Date Created:"/>
				</label>
				${subscriberInstance?.dateCreated}
			</div>
		</div>
		<g:if test="${editing == null || editing == false}">
			<div class="row">
				<div class="fieldcontain ${hasErrors(bean: subscriberInstance, field: 'accepted', 'error')} ">
					<g:checkBox name="accepted" value="${subscriberInstance?.accepted}" /> Do you accept the <a href="${ausgovUrl}/privacy" target="_blank">Terms and Conditions as outlined in the Privacy Policy</a>.
				</div>
			</div>
		</g:if>
		<div class="row">
			<div class="form-group fieldcontain ${hasErrors(bean: subscriberInstance, field: 'subscriptions', 'error')} ">
				<label for="subscriptions" style="width:100%">
					<g:message code="subscriber.subscriptions.label" default="Topics:"/>

				</label>
				<div class="col-md-10">
					<p class="col-md-10" style="padding-left:0px;padding-right:60px;">
						<label class="subscriberCreateBox col-md-10" style="width:100%">
							<g:set var="selectAll" value="${subscriberInstance != null && subscriberInstance.topics != null && subscriberInstance.topics.size() != subscriberpoc.Topic.all.size()}" />
							<g:checkBox id="selectAllCheckBox" name="all" value="Select all" checked="${!selectAll}" /> Select All
						</label>
					</p>
					<g:each in="${subscriberpoc.Topic.list(sort: "name")}" var="topic">
						<label class="subscriberCreateBox col-md-3">
							<g:if test="${subscriberInstance != null && subscriberInstance.topics != null && !subscriberInstance.topics.contains(topic)}">
								<g:checkBox id="" name="topics" value="${topic.id}" checked="${false}" title="${topic.description}" /> ${topic.name}
								<div class="subscriberDescription">${topic.description}</div>
							</g:if>
							<g:else>
								<g:checkBox id="" name="topics" value="${topic.id}" checked="${true}" title="${topic.description}" /> ${topic.name}
								<div class="subscriberDescription">${topic.description}</div>
							</g:else>
						</label>
					</g:each>
				</div>
			</div>
		</div>
	</div>
</section>
</body>