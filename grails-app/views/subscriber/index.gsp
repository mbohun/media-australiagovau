<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subscriber.label', default: 'Subscriber')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
<g:set var="ausgovUrl" value="${grailsApplication.config.australia.gov.au.url}" />
	<body class="not-front">
		<a href="#create-subscriber" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="create-subscriber" class="content scaffold-create" role="main">
			<div class="col-md-4">
				<section class="sidebar clearfix panel-group" id="sidebar-first">
					<div class="region region-sidebar-first">
						<div class="block block-menu-block clearfix" id="block-menu-block-3">
							<div class="panel-heading">
								<h2>In this section</h2>
							</div>
							<div id="s1b-3" class="content panel-collapse collapse">
								<div class="panel-body">
									<div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-3">
										<ul class="menu"><li class="first leaf menu-mlid-1146"><g:link uri="/mediaRelease">Latest media releases</g:link></li>
											<li class="leaf menu-mlid-1149"><a title="Media releases by portfolio" href="${ausgovUrl}/news-and-social-media/media-releases/media-releases-and-rss-feeds-by-portfolio">Media releases and RSS feeds by portfolio</a></li>
											<li class="last leaf menu-mlid-1147"><g:link uri="/subscriber">Subscribe to the Media Release Service</g:link></li>
										</ul></div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div id="main-content" class="col-md-8" role="main">
				<h1>Subscribe for Media Release Notifications </h1>
				<g:if test="${flash.message}">
					<div class="message" role="status">${flash.message}</div>
				</g:if>


				<g:hasErrors bean="${subscriberInstance}">
					<ul class="errors" role="alert">
						<g:eachError bean="${subscriberInstance}" var="error">
							<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}" encodeAs="html"/></li>
						</g:eachError>
					</ul>
				</g:hasErrors>
				<g:form url="[resource:subscriberInstance, action:'signup']" useToken="true" >
					<fieldset class="form">
						<g:render template="index_form"/>
					</fieldset>
					<fieldset class="buttons">

						<g:submitButton name="Subscribe" class="btn btn-primary"
										value="${message(code: 'subscribe.button.create.label', default: 'Subscribe')}"/>
					</fieldset>
				</g:form>
			</div>
			</div>
		</div>
	</body>
</html>
