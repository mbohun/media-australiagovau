<%@ page contentType="text/html"%>
<head>
    <style type="text/css">
    body{padding:40px;font-family: "Open Sans","Lucida Sans",Arial,Helvetica,sans-serif !important;}h1{color:#ca3632;font-weight:600;}h2{color: #00002d;margin-bottom: 0.3em;margin-top: 1.5em;}p.url{color: #16589c;}p.snippet{}
    </style>
</head>
<div>
    <h1>Australian Government Email Subscription Confirmation</h1>

    <p>Please select the link below to confirm your subscription to Australian Government Media Release notifications. If you did not
subscribe you can ignore this email.</p>

    <a href='<g:createLink absolute="true" controller="subscriber" action="confirm" id="${code}" params="[email: email]" />'><g:createLink absolute="true" controller="subscriber" action="confirm" id="${code}" params="[email: email]" /></a>
</div>