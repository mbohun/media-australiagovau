<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title></title>
</head>

<body class="search-body">
<h1>Search Results</h1>
<ul>
    <li><a href="#sites">Sites</a></li>
    <li><a href="#mediarelease">Media Releases</a></li>
    <li><a href="#subscribers">Subscribers</a></li>
    <li><a href="#topicSites">Site with Topic</a></li>
</ul>
<a name="sites"></a>
<h2>Sites</h2>
<g:if test="${sites.size() > 0}">
    <table>
        <thead>
            <tr>
                <th>URL</th>
                <th>Release URL</th>
            </tr>
        </thead>
        <g:each in="${sites}" var="site">
            <tr>
                <td><g:link controller="site" action="show" id="${site.id}">${site.url}</g:link></td>
                <td>${site.releaseUrl}</td>
            </tr>
        </g:each>
    </table>
</g:if>
<g:else>
    <strong>No results found.</strong><br />
</g:else>
<a href="#">Back to Top</a>
<a name="mediarelease"></a>
<h2>Media Releases</h2>
<g:if test="${mediareleases.size() > 0}">
    <table>
        <thead>
        <tr>
            <th>Title</th>
            <th>URL</th>
            <th>Is MediaRelease</th>
        </tr>
        </thead>
        <g:each in="${mediareleases}" var="mediarelease">
            <tr>
                <td><g:link controller="mediaRelease" action="show" id="${mediarelease.id}">${mediarelease.title}</g:link></td>
                <td>${mediarelease.url}</td>
                <td>${mediarelease.isMediaRelease}</td>
            </tr>
        </g:each>
    </table>
</g:if>
<g:else>
    <strong>No results found.</strong><br />
</g:else>
<a href="#">Back to Top</a>
<a name="subscribers"></a>
<h2>Subscribers</h2>
<g:if test="${subscribers.size() > 0}">
    <table>
        <thead>
        <tr>
            <th>Email</th>
            <th>Verified</th>
        </tr>
        </thead>
        <g:each in="${subscribers}" var="subscriber">
            <tr>
                <td><g:link controller="subscriber" action="show" id="${subscriber.id}">${subscriber.email}</g:link></td>
                <td>${subscriber.verified}</td>
            </tr>
        </g:each>
    </table>
</g:if>
<g:else>
    <strong>No results found.</strong><br />
</g:else>
<a href="#">Back to Top</a>
<a name="topicSites"></a>
<h2>Site with Topic
<g:if test="${topicSites != null && topicSites.size() > 0}">
     ${request.getParameter('q')}
    </h2>
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>Url</th>
        </tr>
        </thead>
        <g:each in="${topicSites}" var="site">
            <tr>
                <td>${site.id}</td>
                <td><g:link controller="site" action="show" id="${site.id}">${site.url}</g:link></td>
            </tr>
        </g:each>
    </table>
</g:if>
<g:else>
    </h2>
    <strong>No results found.</strong><br />
</g:else>
<a href="#">Back to Top</a>
</body>
</html>