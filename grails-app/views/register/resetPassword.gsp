<html>

<head>
    <title><g:message code='spring.security.ui.resetPassword.title'/></title>
    <meta name='layout' content='main'/>
</head>

<body>
<g:hasErrors bean="${command}">
    <ul class="errors" role="alert">
        <g:eachError bean="${command}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                    error="${error}"/></li>
        </g:eachError>
    </ul>
</g:hasErrors>

<p>

    <g:message code='spring.security.ui.resetPassword.description'/>

</p>

<div id='login'>
<div class='inner'>

<div class='fheader'>

<g:form action='resetPassword' name='resetPasswordForm' autocomplete='off'>
    <g:hiddenField name='t' value='${token}'/>

    <label for="password">
        <g:message code="user.password.label" default="Password"/>
        <span class="required-indicator">*</span>
    </label>

    <g:passwordField name="password" size="25" type="password" required="true" value="${command?.password}"/>

    <label for="password">
        <g:message code="user.password.label" default="Password (again)"/>
        <span class="required-indicator">*</span>
    </label>


    <g:passwordField name="password2" size="25" required="" value="${command?.password2}"/>
    </div>




    <input type='submit' id="submit" form='resetPasswordForm'
           value='${message(code: "spring.security.ui.resetPassword.submit")}'/>
    </div>
    </div>
</g:form>



<script>
    $(document).ready(function () {
        $('#password').focus();
    });
</script>

</body>
</html>
