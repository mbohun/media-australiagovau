<html>

<head>
<title><g:message code='spring.security.ui.resetPassword.title'/></title>
<meta name='layout' content='main'/>
</head>

<body>

<g:hasErrors bean="${command}">
	<ul class="errors" role="alert">
		<g:eachError bean="${command}" var="error">
			<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
					error="${error}"/></li>
		</g:eachError>
	</ul>
</g:hasErrors>


<p>

	<g:message code='spring.security.ui.forcePassword.description'/>

</p>

<div id='login'>
	<div class='inner'>
		<div class='fheader'>

	<g:form action='resetPassword' name='resetPasswordForm' autocomplete='off'>
	<g:hiddenField name='t' value='${token}'/>



		<s2ui:passwordFieldRow name='password' labelCode='resetPasswordCommand.password.label' bean="${command}"
                             labelCodeDefault='Password' value="${command?.password}"/>

		<s2ui:passwordFieldRow name='password2' labelCode='resetPasswordCommand.password2.label' bean="${command}"
                             labelCodeDefault='Password (again)' value="${command?.password2}"/>


	</div>
		<input type='submit' id="submit" form='resetPasswordForm' value='${message(code: "spring.security.ui.resetPassword.submit")}'/>
	</div>
	</g:form>

</div>

<script>
$(document).ready(function() {
	$('#password').focus();
});
</script>

</body>
</html>
