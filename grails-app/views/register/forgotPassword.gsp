<html>

<head>
    <title><g:message code='spring.security.ui.forgotPassword.title'/></title>
    <meta name='layout' content='main'/>
</head>

<body>

<div id='login'>
    <div class='inner'>
        <div class='fheader'>

            <g:form action='forgotPassword' name="forgotPasswordForm" autocomplete='off'>

            <g:if test='${emailSent}'>
                <br/>
                <g:message code='spring.security.ui.forgotPassword.sent'/>
            </g:if>

            <g:else>

                <p>Forgotten Password</p>
                </div>
                <div class="ftext">
                    Enter your username and we'll send a link to reset your password to the address we have for your account.

                    <label for="username"><g:message code='spring.security.ui.forgotPassword.username'/></label>
                    <g:textField name="username" size="25"/>
                </div>  <p>
                <input type='submit' id="submit" form='forgotPasswordForm'
                       value='${message(code: "spring.security.ui.resetPassword.submit")}'/>
            </p>
            </g:else>

        </g:form>

    </div>
</div>
<script>
    $(document).ready(function () {
        $('#username').focus();
    });
</script>

</body>
</html>
