<html>
<head>
    <meta name='layout' content='main'/>
    <title><g:message code="springSecurity.login.title"/></title>


</head>

<body>

<div id='login'>
    <div class='inner'>
        <div class='fheader'><g:message code='spring.security.ui.login.signin'/></div>

        <g:if test='${flash.message}'>
            <div class='login_message'>${flash.message}</div>
        </g:if>

        <form action='${postUrl}' method='POST' id="loginForm" class="cssform" name="loginForm" autocomplete='off'>

            <p>
                <label for='username'><g:message code='spring.security.ui.login.username'/></label>
                <input type="text" class='text_'  name="j_username" id="username"  />
            </p>

            <p>
                <label for="password"><g:message code='spring.security.ui.login.password'/></label>
                <input type="password" class='text_' name="j_password" id="password"  />
            </p>

            <p>

                <span class="forgot-link">
                    <g:link controller='register' action='forgotPassword'><g:message
                            code='spring.security.ui.login.forgotPassword'/></g:link>
                </span>

            </p>

            <p>
                <input type='submit' id="submit" value='${message(code: "springSecurity.login.button")}'/>
            </p>
         </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#username').focus();
    });

    <s2ui:initCheckboxes/>

</script>

</body>
</html>
