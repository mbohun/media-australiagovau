<!DOCTYPE html>
<html>
	<head>
		<title><g:if env="development">Grails Runtime Exception</g:if><g:else>Error</g:else></title>
		<meta name="layout" content="main">
		<g:if env="development"><asset:stylesheet src="errors.css"/></g:if>
	</head>
	<g:set var="ausgovUrl" value="${grailsApplication.config.australia.gov.au.url}" />
	<body>
		<g:if env="development">
			<g:renderException exception="${exception}" />
		</g:if>
		<g:else>
			<main>
				<!-- #page -->
				<div class="clearfix" id="page">
					<!-- <div class="decoration"></div> -->

					<!-- #main-content -->
					<div id="main-content">
						<div class="container content">
							<div id="page-404">
								<h1><span>An error has occured</span></h1>
								<h2>Get back on track:</h2>
								<ul>
									<li>The address may have changed since you last accessed the page. We recommend you go back to our <a href="${ausgovUrl}/">homepage</a> or try searching our website to find what you're looking for.</li>
									<li>If you typed the address, make sure the spelling is correct.</li>
									<li>You can also go to the <a href="${ausgovUrl}/site-map">site map</a> for an overview of our website.</li>
									<li>If you received this error after clicking on a link from within our website, or if the problem persists, please report the error via our <a href="${ausgovUrl}/contact-us">contact us</a> form.</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- <div class="decoration"></div> -->
				</div>
				<!-- EOF:#page -->
			</main>
		</g:else>
	</body>
</html>
