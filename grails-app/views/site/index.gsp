
<%@ page import="subscriberpoc.Site" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'site.label', default: 'Site')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-site" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-site" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="site.agency.label" default="Agency" /></th>
					
						<g:sortableColumn property="url" title="${message(code: 'site.url.label', default: 'Url')}" />

						<g:sortableColumn property="releaseUrl" title="${message(code: 'site.releaseUrl.label', default: 'Release Url')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${siteInstanceList}" status="i" var="siteInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${siteInstance.id}">${fieldValue(bean: siteInstance, field: "agency")}</g:link></td>
					
						<td>${fieldValue(bean: siteInstance, field: "url")}</td>

						<td>${fieldValue(bean: siteInstance, field: "releaseUrl")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<g:if test="${siteInstanceCount > 10}">
				<div class="pagination">
					<g:paginate total="${siteInstanceCount ?: 0}" />
				</div>
			</g:if>
		</div>
	</body>
</html>
