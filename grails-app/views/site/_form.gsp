<%@ page import="subscriberpoc.Site" %>


<div class="row">
	<div class="fieldcontain ${hasErrors(bean: siteInstance, field: 'agency', 'error')} required">
		<div class="col-md-2">
			<label for="agency">
				<g:message code="site.agency.label" default="Agency" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-10">
			<g:select id="agency" name="agency.id" from="${subscriberpoc.Agency.list()}" optionKey="id" required="" value="${siteInstance?.agency?.id}" class="many-to-one" noSelection="['':'Please select an Agency']"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: siteInstance, field: 'url', 'error')} required">
		<div class="col-md-2">
			<label for="url">
				<g:message code="site.url.label" default="Url" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-10">
			<g:textField name="url" size="100" required="" value="${siteInstance?.url}"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: siteInstance, field: 'topics', 'error')} ">
		<div class="col-md-2">
			<label for="topics">
				<g:message code="site.topics.label" default="Topics" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-2">
			<g:select name="topics" from="${subscriberpoc.Topic.list()}" multiple="multiple" required="" optionKey="id" size="5" value="${siteInstance?.topics*.id}" class="many-to-many"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: siteInstance, field: 'description', 'error')} ">
		<div class="col-md-2">
			<label for="description">
				<g:message code="site.description.label" default="Description" />
			</label>
		</div>
		<div class="col-md-10">
			<g:textField name="description" size="100" value="${siteInstance?.description}"/>
			<p class="info">This field is used for better results for descriptions using <a href="http://jsoup.org/cookbook/extracting-data/selector-syntax" target="_blank">selector syntax</a> <strong>(Currently not used)</strong>.</p>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: siteInstance, field: 'mediaReleaseSelector', 'error')} ">
		<div class="col-md-2">
			<label for="mediaReleaseSelector">
				<g:message code="site.mediaReleaseSelector.label" default="Media Release Selector" />
			</label>
		</div>
		<div class="col-md-10">
			<g:textField name="mediaReleaseSelector" size="100" value="${siteInstance?.mediaReleaseSelector}"/>
			<p class="info">This field is used for finer grained results using <a href="http://jsoup.org/cookbook/extracting-data/selector-syntax" target="_blank">selector syntax</a> <strong>(Currently not used)</strong>.</p>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: siteInstance, field: 'releaseUrl', 'error')} ">
		<div class="col-md-2">
			<label for="releaseUrl">
				<g:message code="site.releaseUrl.label" default="Release Url" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-10">
			<g:textField name="releaseUrl" size="100" required="" value="${siteInstance?.releaseUrl}"/>
		</div>
	</div>
</div>


