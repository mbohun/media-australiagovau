
<%@ page import="subscriberpoc.Site" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'site.label', default: 'Site')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-site" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-site" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list site">
			
				<g:if test="${siteInstance?.agency}">
				<li class="fieldcontain">
					<span id="agency-label" class="property-label"><g:message code="site.agency.label" default="Agency" /></span>
					
						<span class="property-value" aria-labelledby="agency-label"><g:link controller="agency" action="show" id="${siteInstance?.agency?.id}">${siteInstance?.agency?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${siteInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="site.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${siteInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${siteInstance?.topics}">
				<li class="fieldcontain">
					<span id="topics-label" class="property-label"><g:message code="site.topics.label" default="Topics" /></span>
					
						<g:each in="${siteInstance.topics}" var="t">
						<span class="property-value" aria-labelledby="topics-label"><g:link controller="topic" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>

				<g:if test="${siteInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="site.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${siteInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${siteInstance?.mediaReleaseSelector}">
				<li class="fieldcontain">
					<span id="mediaReleaseSelector-label" class="property-label"><g:message code="site.mediaReleaseSelector.label" default="Media Release Selector" /></span>
					
						<span class="property-value" aria-labelledby="mediaReleaseSelector-label"><g:fieldValue bean="${siteInstance}" field="mediaReleaseSelector"/></span>
					
				</li>
				</g:if>

				<g:if test="${siteInstance?.releaseUrl}">
					<li class="fieldcontain">
						<span id="releaseUrl-label" class="property-label"><g:message code="site.releaseUrl.label" default="Release Url" /></span>

						<span class="property-value" aria-labelledby="releaseUrl-label"><g:fieldValue bean="${siteInstance}" field="releaseUrl"/></span>

					</li>
				</g:if>

	<%--			<g:if test="${siteInstance?.releases}">
				<li class="fieldcontain">
					<span id="releases-label" class="property-label"><g:message code="site.releases.label" default="Releases" /></span>

						<g:each in="${siteInstance.releases}" var="r">
						<span class="property-value" aria-labelledby="releases-label"><g:link controller="mediaRelease" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
						</g:each>

				</li>
				</g:if> --%>

			</ol>
			<g:form url="[resource:siteInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${siteInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
