<%--
  Created by IntelliJ IDEA.
  User: dean
  Date: 20/08/15
  Time: 3:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title>Testing only</title>
</head>

<body>
     <h1>Job Admin</h1>
    <g:if test="${status}">
        <div id="status">
            ${status}
        </div>
    </g:if>
<g:form action="show">
    <fieldset>
        <legend>Job Admin Operations</legend>
        <label for="operation">Select a Crawler operation:</label>
        <g:select id="operation" name="operation" from="${['Fire Crawler','Pause Crawler', 'Fire Emailer', 'Fire Expirer']}"/>

        <g:submitButton name="go" value="Go"/>
    </fieldset>



</g:form>
</body>
</html>