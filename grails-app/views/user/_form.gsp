<%@ page import="subscriberpoc.User" %>

<div class="row">
	<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
		<div class="col-md-2">
			<label for="username">
				<g:message code="user.username.label" default="Username" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-10">
			<g:textField name="username" size="100" required="" value="${userInstance?.username}"/>
		</div>
	</div>
</div>
%{--<div class="row">--}%
	%{--<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'error')} required">--}%
		%{--<div class="col-md-2">--}%
			%{--<label for="password">--}%
				%{--<g:message code="user.password.label" default="Password" />--}%
				%{--<span class="required-indicator">*</span>--}%
			%{--</label>--}%
		%{--</div>--}%
		%{--<div class="col-md-10">--}%
			%{--<g:textField name="password" size="100" required="" value="${userInstance?.password}"/>--}%
		%{--</div>--}%
	%{--</div>--}%
%{--</div>--}%
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'email', 'error')} required">
		<div class="col-md-2">
			<label for="email">
				<g:message code="user.email.label" default="Email" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-10">
			<g:field type="email" name="email" size="100" required="" value="${userInstance?.email}"/>
		</div>
	</div>
</div>
%{--<div class="row">--}%
	%{--<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'accountExpired', 'error')} ">--}%
		%{--<div class="col-md-2">--}%
			%{--<label for="accountExpired">--}%
				%{--<g:message code="user.accountExpired.label" default="Account Expired" />--}%
			%{--</label>--}%
		%{--</div>--}%
		%{--<div class="col-md-10">--}%
			%{--<g:checkBox name="accountExpired" value="${userInstance?.accountExpired}" />--}%
		%{--</div>--}%
	%{--</div>--}%
%{--</div>--}%
%{--<div class="row">--}%
	%{--<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'accountLocked', 'error')} ">--}%
		%{--<div class="col-md-2">--}%
			%{--<label for="accountLocked">--}%
				%{--<g:message code="user.accountLocked.label" default="Account Locked" />--}%
			%{--</label>--}%
		%{--</div>--}%
		%{--<div class="col-md-10">--}%
			%{--<g:checkBox name="accountLocked" value="${userInstance?.accountLocked}" />--}%
		%{--</div>--}%
	%{--</div>--}%
%{--</div>--}%
%{--<div class="row">--}%
	%{--<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'enabled', 'error')} ">--}%
		%{--<div class="col-md-2">--}%
			%{--<label for="enabled">--}%
				%{--<g:message code="user.enabled.label" default="Enabled" />--}%
			%{--</label>--}%
		%{--</div>--}%
		%{--<div class="col-md-10">--}%
			%{--<g:checkBox name="enabled" value="${userInstance?.enabled}" />--}%
		%{--</div>--}%
	%{--</div>--}%
%{--</div>--}%
%{--<div class="row">--}%
	%{--<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'passwordExpired', 'error')} ">--}%
		%{--<div class="col-md-2">--}%
			%{--<label for="passwordExpired">--}%
				%{--<g:message code="user.passwordExpired.label" default="Password Expired" />--}%
			%{--</label>--}%
		%{--</div>--}%
		%{--<div class="col-md-10">--}%
			%{--<g:checkBox name="passwordExpired" value="${userInstance?.passwordExpired}" />--}%
		%{--</div>--}%
	%{--</div>--}%
%{--</div>--}%
