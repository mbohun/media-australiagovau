
<%@ page import="subscriberpoc.MediaRelease" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="Latest media releases" />
		<title>${entityName}</title>
	</head>
	<g:set var="ausgovUrl" value="${grailsApplication.config.australia.gov.au.url}" />
	<body class="not-front">
		<a href="#list-mediaRelease" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div class="col-md-4">
			<section class="sidebar clearfix panel-group" id="sidebar-first">
				<div class="region region-sidebar-first">
					<div class="block block-menu-block clearfix" id="block-menu-block-3">
						<div class="panel-heading">
							<h2>In this section</h2>
						</div>
						<div id="s1b-3" class="content panel-collapse collapse">
							<div class="panel-body">
								<div class="menu-block-wrapper menu-block-3 menu-name-main-menu parent-mlid-0 menu-level-3">
									<ul class="menu"><li class="first leaf menu-mlid-1146"><g:link uri="/mediaRelease">Latest media releases</g:link></li>
										<li class="leaf menu-mlid-1149"><a title="Media releases by portfolio" href="${ausgovUrl}/news-and-social-media/media-releases/media-releases-and-rss-feeds-by-portfolio">Media releases and RSS feeds by portfolio</a></li>
										<li class="last leaf menu-mlid-1147"><g:link uri="/subscriber">Subscribe to the Media Release Service</g:link></li>
									</ul></div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div id="main-content" class="col-md-8" role="main">
			<h1>${entityName}</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<div class="media-release-results-list">
				<p>
					This is a list of the latest media releases from Australian Government websites arranged under their portfolios. It is generated daily from new media releases collected overnight.
				</p>
				<g:each in="${mediaReleaseInstanceList}" status="i" var="mediaReleaseInstance">

					<h2><a href="${fieldValue(bean: mediaReleaseInstance, field: "url")}">${fieldValue(bean: mediaReleaseInstance, field: "title")}</a></h2>
					<div class="media-release-snippet">
						${fieldValue(bean: mediaReleaseInstance, field: "snippet")}
						<sec:ifLoggedIn>
							<g:link action="show" id="${mediaReleaseInstance.id}" controller="mediaRelease">Edit</g:link>
						</sec:ifLoggedIn>
					</div>
					<div class="media-release-url">
						<p class="r_url">
							${fieldValue(bean: mediaReleaseInstance, field: "url")}
						</p>
					</div>
				</g:each>
			</div>
		</div>
	</body>
</html>
