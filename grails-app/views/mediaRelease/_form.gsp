<%@ page import="subscriberpoc.MediaRelease" %>

<div class="row">
	<div class="fieldcontain ${hasErrors(bean: mediaReleaseInstance, field: 'title', 'error')} required">
		<div class="col-md-2">
			<label for="title">
				<g:message code="mediaRelease.title.label" default="Title" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-10">
			<g:textField name="title" required="" size="100" value="${mediaReleaseInstance?.title}"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: mediaReleaseInstance, field: 'url', 'error')} ">
		<div class="col-md-2">
			<label for="url">
				<g:message code="mediaRelease.url.label" default="Url" />
			</label>
		</div>
		<div class="col-md-10">
			<g:field type="url" name="url" size="100" value="${mediaReleaseInstance?.url}"/>
		</div>
	</div>
</div>
<div class="row">
    <div class="fieldcontain ${hasErrors(bean: mediaReleaseInstance, field: 'shortUrl', 'error')} ">
        <div class="col-md-2">
            <label for="shortUrl">
                <g:message code="mediaRelease.shortUrl.label" default="shortUrl" />
            </label>
        </div>
        <div class="col-md-10">
            <g:field type="url" name="shortUrl" size="100" value="${mediaReleaseInstance?.shortUrl}"/>
        </div>
    </div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: mediaReleaseInstance, field: 'snippet', 'error')} ">
		<div class="col-md-2">
			<label for="snippet">
				<g:message code="mediaRelease.snippet.label" default="Snippet" />
			</label>
		</div>
		<div class="col-md-10">
			<g:textArea name="snippet" cols="100" rows="3" maxlength="1024" style="width:555px;height: 100px;" value="${mediaReleaseInstance?.snippet}"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: mediaReleaseInstance, field: 'hasBeenSent', 'error')} ">
		<div class="col-md-2">
			<label for="hasBeenSent">
				<g:message code="mediaRelease.hasBeenSent.label" default="Has Been Sent" />
			</label>
		</div>
		<div class="col-md-10">
			<g:checkBox name="hasBeenSent" value="${mediaReleaseInstance?.hasBeenSent}" />
			<p class="info">Variable used to signal that a media release has been emailed to relevant subscribers. <strong>If selected won't appear on approve list for e-mails</strong></p>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: mediaReleaseInstance, field: 'isMediaRelease', 'error')} ">
		<div class="col-md-2">
			<label for="isMediaRelease">
				<g:message code="mediaRelease.isMediaRelease.label" default="Is Media Release" />
			</label>
		</div>
		<div class="col-md-10">
			<g:checkBox name="isMediaRelease" value="${mediaReleaseInstance?.isMediaRelease}" />
			<p class="info">Variable used to confirm that auto-fetched releases are in fact Media Releases. <strong>Can set here, or on the approve list</strong></p>
		</div>
	</div>
</div>
<div class="row">
	<div class="fieldcontain ${hasErrors(bean: mediaReleaseInstance, field: 'site', 'error')} required">
		<div class="col-md-2">
			<label for="site">
				<g:message code="mediaRelease.site.label" default="Site" />
				<span class="required-indicator">*</span>
			</label>
		</div>
		<div class="col-md-10">
			<g:select id="site" name="site.id" from="${subscriberpoc.Site.list()}" optionKey="id" required="" value="${mediaReleaseInstance?.site?.id}" class="many-to-one" noSelection="['':'Please select a Site']"/>
		</div>
	</div>
</div>

