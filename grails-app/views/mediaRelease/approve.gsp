
<%@ page import="subscriberpoc.MediaRelease" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'mediaRelease.label', default: 'MediaRelease')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-mediaRelease" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="list-mediaRelease" class="content scaffold-list" role="main">
    <h1>Please select links that are actually media releases </h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>


    <g:if test="${mediaReleaseInstanceCount > 0}">
        <div class="selectAllApproveBox" style="width:100%;padding-bottom:30px;">
            <span style="float:right">
                <button id="selectAllApprove" name="all">Select All</button>
            </span>
        </div>
        %{--<g:form url="[resource: mediaReleaseInstanceList, action:'send']" >--}%
        <g:form action="send">
            <table>
                <thead>
                <tr>
                    <th>#</th>
                    <g:sortableColumn property="url" title="${message(code: 'mediaRelease.url.label', default: 'Url')}" />
                    <g:sortableColumn property="isMediaRelease" title="${message(code: 'mediaRelease.isMediaRelease.label', default: 'Is Media Release')}" />
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${mediaReleaseInstanceList}" status="i" var="mediaReleaseInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                        <td style="padding-right: 5px;">${i + 1}</td>
                        <td>
                            <a href='${fieldValue(bean: mediaReleaseInstance, field: "url")}' target="_blank">${fieldValue(bean: mediaReleaseInstance, field: "url")}</a><br />
                            <g:if test='${fieldValue(bean: mediaReleaseInstance, field: "shortUrl")}'>
                                <a href='${fieldValue(bean: mediaReleaseInstance, field: "shortUrl")}' target="_blank">${fieldValue(bean: mediaReleaseInstance, field: "shortUrl")}</a><br />
                            </g:if>
                            <p class="info"><a onclick="showHide(${i + 1})">${fieldValue(bean: mediaReleaseInstance, field: "title")}</a></p>
                            <p class="info hide" id="description${i + 1}">${fieldValue(bean: mediaReleaseInstance, field: "snippet")}</p>
                        </td>
                        <td style="vertical-align:middle;text-align: center"><g:checkBox name="release" value="${mediaReleaseInstance.id}" checked="${mediaReleaseInstance.isMediaRelease}"/> <g:hiddenField name="releaseId" value="${mediaReleaseInstance.id}" /></td>
                        <td style="vertical-align:middle;"><g:link class="edit" action="edit" resource="${mediaReleaseInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link></td>
                    </tr>
                </g:each>
                </tbody>
            </table>

            <fieldset class="buttons">
                <g:submitButton name="Send" class="btn btn-primary" value="Send emails" />
            </fieldset>

        </g:form>
    </g:if>
    <g:else>
        <div>There are no media releases ready to be processed and emailed. Please wait for the 2am job to run. <p class="info">(Usually complete by 4:30am AEST)</p></div>
    </g:else>
</div>

</body>
</html>

