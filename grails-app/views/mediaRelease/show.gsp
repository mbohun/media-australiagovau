
<%@ page import="subscriberpoc.MediaRelease" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mediaRelease.label', default: 'MediaRelease')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-mediaRelease" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-mediaRelease" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list mediaRelease">
			
				<g:if test="${mediaReleaseInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="mediaRelease.title.label" default="Title" /></span>
					
						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${mediaReleaseInstance}" field="title"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaReleaseInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="mediaRelease.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${mediaReleaseInstance}" field="url"/></span>
					
				</li>
				</g:if>

                <!-- NOTE: this could in theory, and maybe even more correctly, be inside the above g:if test url block,
                           because if there is NO (original/long) URL, there is obviously no shortUrl either.
                   -->
                <g:if test="${mediaReleaseInstance?.shortUrl}">
                <li class="fieldcontain">
                    <span id="short-url-label" class="property-label"><g:message code="mediaRelease.shortUrl.label" default="shortUrl" /></span>

                        <span class="property-value" aria-labelledby="short-url-label"><g:fieldValue bean="${mediaReleaseInstance}" field="shortUrl"/></span>

                </li>
                </g:if>

				<g:if test="${mediaReleaseInstance?.snippet}">
				<li class="fieldcontain">
					<span id="snippet-label" class="property-label"><g:message code="mediaRelease.snippet.label" default="Snippet" /></span>
					
						<span class="property-value" aria-labelledby="snippet-label"><g:fieldValue bean="${mediaReleaseInstance}" field="snippet"/></span>
					
				</li>
				</g:if>

				<g:if test="${mediaReleaseInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="mediaRelease.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${mediaReleaseInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaReleaseInstance?.hasBeenSent}">
				<li class="fieldcontain">
					<span id="hasBeenSent-label" class="property-label"><g:message code="mediaRelease.hasBeenSent.label" default="Has Been Sent" /></span>
					
						<span class="property-value" aria-labelledby="hasBeenSent-label"><g:formatBoolean boolean="${mediaReleaseInstance?.hasBeenSent}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaReleaseInstance?.isMediaRelease}">
				<li class="fieldcontain">
					<span id="isMediaRelease-label" class="property-label"><g:message code="mediaRelease.isMediaRelease.label" default="Is Media Release" /></span>
					
						<span class="property-value" aria-labelledby="isMediaRelease-label"><g:formatBoolean boolean="${mediaReleaseInstance?.isMediaRelease}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaReleaseInstance?.site}">
				<li class="fieldcontain">
					<span id="site-label" class="property-label"><g:message code="mediaRelease.site.label" default="Site" /></span>
					
						<span class="property-value" aria-labelledby="site-label"><g:link controller="site" action="show" id="${mediaReleaseInstance?.site?.id}">${mediaReleaseInstance?.site?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:mediaReleaseInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${mediaReleaseInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
