<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <title></title>
    <gvisualization:apiImport/>
</head>

<body class="statistics-body">
<ul>
    <li><a href="#subscribersPerTopic">Subscribers per Topic</a> | <g:link absolute="true" controller="statistics" action="export_subscriber_per_topic">Export</g:link></li>
    <li><a href="#mediaReleasesPerSite">Media Releases per Site</a> | <g:link absolute="true" controller="statistics" action="export_media_releases_per_site">Export</g:link></li>
    <li><a href="#mediaReleasesPerTopic">Media Releases per Topic</a> | <g:link absolute="true" controller="statistics" action="export_media_releases_per_topic">Export</g:link></li>
    <li><a href="#sitesLessThanOneRelease">Sites with one or less releases</a> | <g:link absolute="true" controller="statistics" action="export_site_less_than_one_release">Export</g:link></li>
    <li><a href="#sitesLastUpdated">Sites last updated</a> | <g:link absolute="true" controller="statistics" action="export_site_last_updated">Export</g:link></li>
</ul>

<gvisualization:columnCoreChart hAxis="${new Expando([title: 'Topics'])}"
                              vAxis="${new Expando([title: 'Subscribers', viewWindowMode: 'explicit', viewWindow: new Expando([min: 0])])}"
                              elementId="barchartSubscribersPerTopic"
                              width="${1400}"
                              height="${600}"
                              title="Subscribers per Topic"
                              columns="${graphDataColumnsTtoS}"
                              data="${graphDataTtoS}" />
<a name="subscribersPerTopic"></a>
<a href="#">Back to top</a>
<div id="barchartSubscribersPerTopic"></div>


<gvisualization:columnCoreChart hAxis="${new Expando([title: 'Sites'])}"
                                vAxis="${new Expando([title: 'Media Releases', viewWindowMode: 'explicit', viewWindow: new Expando([min: 0])])}"
                                elementId="barchartMediaReleasesPerSite"
                                width="${1400}"
                                height="${600}"
                                title="Media Releases per Site"
                                columns="${graphDataColumnsMRtoS}"
                                data="${graphDataMRtoS}" />
<a name="mediaReleasesPerSite"></a>
<a href="#">Back to top</a>
<div id="barchartMediaReleasesPerSite"></div>

<gvisualization:columnCoreChart hAxis="${new Expando([title: 'Topics'])}"
                                vAxis="${new Expando([title: 'Media Releases', viewWindowMode: 'explicit', viewWindow: new Expando([min: 0])])}"
                                elementId="barchartMediaReleasesPerTopic"
                                width="${1400}"
                                height="${600}"
                                title="Media Releases per Topic"
                                columns="${graphDataColumnsMRtoT}"
                                data="${graphDataMRtoT}" />
<a name="mediaReleasesPerTopic"></a>
<a href="#">Back to top</a>
<div id="barchartMediaReleasesPerTopic"></div>

<a name="sitesLessThanOneRelease"></a>
<h4>Sites with one or less releases</h4>
<a href="#">Back to top</a>
<div id="sitesLessThanOneReleaseDiv">
    <table>
        <tr>
            <th>Site</th>
            <th>Count</th>
        </tr>
        <g:each in="${sitesWithOneOrLess}" var="site">
            <tr>
            <td><g:link controller="site" action="show" id="${site[3]}">${site[0]}</g:link></td>
            <td>${site[1]} (${site[2]})</td>
            </tr>
        </g:each>
    </table>
</div>

<a name="sitesLastUpdated"></a>
<h4>Sites last updated</h4>
<a href="#">Back to top</a>
<div id="sitesLastUpdatedDiv">
    <table>
        <tr>
            <th>Site</th>
            <th>Last Updated</th>
        </tr>
        <g:each in="${sitesLastUpdated}" var="site">
            <tr>
                <td><g:link controller="site" action="show" id="${site[3]}">${site[0]}</g:link></td>
                <td>${site[1]} (${site[2]})</td>
            </tr>
        </g:each>
    </table>
</div>

</body>
</html>