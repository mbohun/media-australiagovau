<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link type="image/png" href="${ausgovUrl}/profiles/gsecms/themes/ausgovau_bootstrap/favicons/favicon-16x16.png" rel="shortcut icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
	<g:set var="ausgovUrl" value="${grailsApplication.config.australia.gov.au.url}" />
	<body class="html ${pageProperty(name: 'body.class')}">
		<sec:ifLoggedIn>
			<nav class="navbar navbar-default navbar-fixed-top logged-in" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Media Releases</a>
					</div>
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Agency <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><g:link controller="agency">List</g:link></li>
								<li><g:link controller="agency" action="create">Create Agency</g:link></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MediaRelease <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><g:link controller="mediaRelease">List</g:link></li>
								<li><g:link controller="mediaRelease" action="approve">Approve Releases</g:link></li>
								<li role="separator" class="divider">-------------------------------------</li>
								<li><g:link controller="mediaRelease" action="create">Create Media Release</g:link></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Site <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><g:link controller="site">List</g:link></li>
								<li><g:link controller="site" action="create">Create Site</g:link></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Subscriber <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><g:link controller="subscriber" action="list">List</g:link></li>
								<li><g:link controller="subscriber">Create New Subscriber</g:link></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Topic <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><g:link controller="topic">List</g:link></li>
								<li><g:link controller="topic" action="create">Create Topic</g:link></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><g:link controller="user">List</g:link></li>
								<li><g:link controller="user" action="create">Create User</g:link></li>
							</ul>
						</li>
						<li>
							<g:link controller="statistics">Statistics</g:link>
						</li>
						<li style="display: none">
							<g:link controller="jobAdmin">Job Admin</g:link>
						</li>
					</ul>
					<g:form class="navbar-form navbar-left" role="search" url="[action:'index',controller:'search']" method="GET">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" name="q" id="q">
						</div>
						<button type="submit" class="btn btn-default">Search</button>
					</g:form>
					<ul class="nav navbar-nav navbar-right" style="height:61px">
						<li>
							<g:form name="logoutForm" controller="logout" action="index">
								Currently logged in as <sec:username/>
								<g:submitButton name="signOut" value="Sign Out"/>
							</g:form>
						</li>
					</ul>
				</div>
			</nav>
		</sec:ifLoggedIn>
		<sec:ifNotLoggedIn>
			<div class="loginFormClass">
				<g:form name="loginForm" controller="login" action="index" style="position:absolute;right:0">
					<g:submitButton name="login" value="Log In"/>
				</g:form>
			</div>
		</sec:ifNotLoggedIn>
		<div id="main-body">
			<header class="clearfix" role="banner" id="header">
				<div class="container">
					<a rel="home" title="Home" href="${ausgovUrl}/" id="crest">Australian Government</a>
					<div title="Home" id="site-title">
						<a href="${ausgovUrl}">Australia.gov.au</a>
					</div>
				</div>
			</header>

			<nav id="nav-primary" role="navigation">
				<ul class="menu"><li class="first leaf"><i class="icon icon-home icon-tid-22"></i><a id="nav-item-home" href="${ausgovUrl}/.">Home</a></li>
					<li class="expanded"><i class="icon icon-information-and-services icon-tid-25"></i><a id="nav-item-information-and-services" href="${ausgovUrl}/information-and-services">Information and Services</a><div id="nav-sub-menu-information-and-services" class="sub-menu"><div class="row"><div class="col-md-3 col-sm-3"><ul><li class="mobile-only"><a class="sub-menu-back" href="#">Close</a></li><li class="first collapsed"><i class="icon icon-benefits-and-payments icon-tid-67"></i><a id="nav-item-benefits-and-payments" href="${ausgovUrl}/information-and-services/benefits-and-payments">Benefits and Payments</a></li>
						<li class="collapsed"><i class="icon icon-environment icon-tid-74"></i><a id="nav-item-environment" href="${ausgovUrl}/information-and-services/environment">Environment</a></li>
						<li class="collapsed"><i class="icon icon-it-and-communications icon-tid-79"></i><a id="nav-item-it-and-communications" href="${ausgovUrl}/information-and-services/it-and-communications">IT and Communications</a></li>
						<li class="collapsed"><i class="icon icon-public-safety-and-law icon-tid-80"></i><a id="nav-item-public-safety-and-law" href="${ausgovUrl}/information-and-services/public-safety-and-law">Public Safety and Law</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-business-and-industry icon-tid-68"></i><a id="nav-item-business-and-industry" href="${ausgovUrl}/information-and-services/business-and-industry">Business and Industry</a></li>
						<li class="collapsed"><i class="icon icon-family-and-community icon-tid-75"></i><a id="nav-item-family-and-community" href="${ausgovUrl}/information-and-services/family-and-community">Family and Community</a></li>
						<li class="collapsed"><i class="icon icon-jobs-and-workplace icon-tid-73"></i><a id="nav-item-jobs-and-workplace" href="${ausgovUrl}/information-and-services/jobs-and-workplace">Jobs and Workplace</a></li>
						<li class="collapsed"><i class="icon icon-security-and-defence icon-tid-70"></i><a id="nav-item-security-and-defence" href="${ausgovUrl}/information-and-services/security-and-defence">Security and Defence</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-culture-and-arts icon-tid-69"></i><a id="nav-item-culture-and-arts" href="${ausgovUrl}/information-and-services/culture-and-arts">Culture and Arts</a></li>
						<li class="collapsed"><i class="icon icon-health icon-tid-77"></i><a id="nav-item-health" href="${ausgovUrl}/information-and-services/health">Health</a></li>
						<li class="collapsed"><i class="icon icon-money-and-tax icon-tid-71"></i><a id="nav-item-money-and-tax" href="${ausgovUrl}/information-and-services/money-and-tax">Money and Tax</a></li>
						<li class="collapsed"><i class="icon icon-transport-and-regional icon-tid-83"></i><a id="nav-item-transport-and-regional" href="${ausgovUrl}/information-and-services/transport-and-regional">Transport and Regional</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-education-and-training icon-tid-72"></i><a id="nav-item-education-and-training" href="${ausgovUrl}/information-and-services/education-and-training">Education and Training</a></li>
						<li class="collapsed"><i class="icon icon-immigration-and-visas icon-tid-78"></i><a id="nav-item-immigration-and-visas" href="${ausgovUrl}/information-and-services/immigration-and-visas">Immigration and Visas</a></li>
						<li class="collapsed"><i class="icon icon-passports-and-travel icon-tid-381"></i><a id="nav-item-passports-and-travel" href="${ausgovUrl}/information-and-services/passports-and-travel">Passports and Travel</a></li>
						<li class="last leaf"><i class="icon icon-a-z-of-government-services icon-tid-39"></i><a id="nav-item-a-z-of-government-services" href="${ausgovUrl}/information-and-services/a-z-of-government-services">A-Z of Government Services</a></li>
					</ul></div></div></div></li>
					<li class="expanded"><i class="icon icon-about-government icon-tid-28"></i><a id="nav-item-about-government" href="${ausgovUrl}/about-government">About Government</a><div id="nav-sub-menu-about-government" class="sub-menu"><div class="row"><div class="col-md-3 col-sm-3"><ul><li class="mobile-only"><a class="sub-menu-back" href="#">Close</a></li><li class="first leaf"><i class="icon icon-contact-government icon-tid-102"></i><a id="nav-item-contact-government" href="${ausgovUrl}/about-government/contact-government">Contact Government</a></li>
						<li class="collapsed"><i class="icon icon-publications icon-tid-29"></i><a id="nav-item-publications" href="${ausgovUrl}/about-government/publications">Publications</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-departments-and-agencies icon-tid-104"></i><a id="nav-item-departments-and-agencies" href="${ausgovUrl}/about-government/departments-and-agencies">Departments and Agencies</a></li>
						<li class="leaf"><i class="icon icon-apps icon-tid-447"></i><a id="nav-item-apps" href="${ausgovUrl}/about-government/apps">Apps</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-government-and-parliament icon-tid-76"></i><a id="nav-item-government-and-parliament" href="${ausgovUrl}/about-government/government-and-parliament">Government and Parliament</a></li>
						<li class="collapsed"><i class="icon icon-states-territories-and-local-government icon-tid-439"></i><a id="nav-item-states-territories-and-local-government" href="${ausgovUrl}/about-government/states-territories-and-local-government">States, Territories and Local Government</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-international-relations icon-tid-228"></i><a id="nav-item-international-relations" href="${ausgovUrl}/about-government/international-relations">International Relations</a></li>
						<li class="last collapsed"><i class="icon icon-how-government-works icon-tid-99"></i><a id="nav-item-how-government-works" href="${ausgovUrl}/about-government/how-government-works">How Government Works</a></li>
					</ul></div></div></div></li>
					<li class="expanded in_active_trail"><i class="icon icon-news-and-social-media icon-tid-30"></i><a id="nav-item-news-and-social-media" href="${ausgovUrl}/news-and-social-media">News and Social Media</a><div id="nav-sub-menu-news-and-social-media" class="sub-menu"><div class="row"><div class="col-md-3 col-sm-3"><ul><li class="mobile-only"><a class="sub-menu-back" href="#">Close</a></li><li class="first collapsed in_active_trail"><i class="icon icon-media-releases icon-tid-114"></i><a class="active" id="nav-item-media-releases" href="${ausgovUrl}/news-and-social-media/media-releases">Media Releases</a></li>
						<li class="last leaf"><i class="icon icon-public-consultations icon-tid-112"></i><a id="nav-item-public-consultations" href="${ausgovUrl}/news-and-social-media/public-consultations">Public consultations</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-social-media icon-tid-113"></i><a id="nav-item-social-media" href="${ausgovUrl}/news-and-social-media/social-media">Social Media</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="leaf"><i class="icon icon-campaigns icon-tid-116"></i><a id="nav-item-campaigns" href="${ausgovUrl}/news-and-social-media/campaigns">Campaigns</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="leaf"><i class="icon icon-what-s-on icon-tid-115"></i><a id="nav-item-what-s-on" href="${ausgovUrl}/news-and-social-media/whats-on">What's On</a></li>
					</ul></div></div></div></li>
					<li class="expanded"><i class="icon icon-about-australia icon-tid-27"></i><a id="nav-item-about-australia" href="${ausgovUrl}/about-australia">About Australia</a><div id="nav-sub-menu-about-australia" class="sub-menu"><div class="row"><div class="col-md-3 col-sm-3"><ul><li class="mobile-only"><a class="sub-menu-back" href="#">Close</a></li><li class="first collapsed"><i class="icon icon-facts-and-figures icon-tid-66"></i><a id="nav-item-facts-and-figures" href="${ausgovUrl}/about-australia/facts-and-figures">Facts and Figures</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-special-dates-and-events icon-tid-174"></i><a id="nav-item-special-dates-and-events" href="${ausgovUrl}/about-australia/special-dates-and-events">Special dates and events</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="collapsed"><i class="icon icon-our-country icon-tid-98"></i><a id="nav-item-our-country" href="${ausgovUrl}/about-australia/our-country">Our country</a></li>
					</ul></div><div class="col-md-3 col-sm-3"><ul><li class="last collapsed"><i class="icon icon-australian-stories icon-tid-100"></i><a id="nav-item-australian-stories" href="${ausgovUrl}/about-australia/australian-stories">Australian Stories</a></li>
					</ul></div></div></div></li>
					<li class="last leaf"><i class="icon icon-mygov icon-tid-3746"></i><a id="nav-item-mygov" href="${ausgovUrl}/mygov">myGov</a></li>
				</ul>
			</nav>
			<main>
				<div id="page" class="clearfix">
					<section class="decoration" data-speed="2" data-type="background">
					</section>
					<div id="main-content">
						<div class="container">
							<!-- #messages-console -->
							<!--<div id="messages-console" class="clearfix">
								<div class="row">
									<div class="col-md-12">
									 //Message here
									</div>
								</div>
							</div>-->


							<div class="row">

								<!-- #breadcrumb -->
								<div id="breadcrumb" class="clearfix">
									<!-- #breadcrumb-inside -->
									<div id="breadcrumb-inside" class="clearfix">
										<ol class="breadcrumb">
											<li><a href="${ausgovUrl}/">Home</a></li>
											<li><a title="News and Social Media" href="${ausgovUrl}/news-and-social-media">News and Social Media</a></li>
											<li><a title="Media Releases" href="${ausgovUrl}/news-and-social-media/media-releases">Media Releases</a></li>
										</ol>
									</div>
									<!-- EOF: #breadcrumb-inside -->
								</div>
								<!-- EOF: #breadcrumb -->
								<!--Readspeak?-->
								<section id="top" class="col-md-12">
									<!-- #main -->
									<div id="main" class="clearfix">
										<!-- EOF:#content-wrapper -->
										<div id="content-wrapper">
											<g:layoutBody/>
										</div>
										<!-- EOF:#content-wrapper -->
									</div>
									<!-- EOF:#main -->
								</section>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
