package subscriberpoc

import org.apache.commons.logging.LogFactory


class EmailerJob {

    private static final log = LogFactory.getLog(this)
    def concurrent = false
    def dailyEmailerService
    def group = "dailyEmailerService"

    static triggers = {

    }

    def execute(context) {
        log.info("Preparing dailyEmailService to send emails")
        sleep(5000)
        List releases = context.mergedJobDataMap.get('releases')
        dailyEmailerService.send(releases)

    }
}
