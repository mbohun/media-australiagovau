package subscriberpoc

import org.apache.commons.logging.LogFactory


class PwdExpirerJob {

    private static final log = LogFactory.getLog(this)

    def grailsApplication
    def concurrent = false

    static triggers = {
        cron cronExpression: "0 0 1 ? * *" // run daily at 1am
    }

    def group = "expirerServices"

    def execute() {
        def final timeNow = new Date()
        log.info("PwdExpirer has been fired: " + timeNow) //NOTE: we could re-name this job to (more generic) "Expirer"

        def final expiredDateUser = timeNow - grailsApplication.config.grails.expiryPeriod
        //TODO: well you can do this, but one might ask why not simply use GORM and and pull over
        //      *ONLY* the users where (user.lastPwdChange < expiredDate) and expire those
        def final users = User.findAll()
        users.each{ user ->
            if (user.lastPwdChange < expiredDateUser){
                user.passwordExpired = true
                user.save()
            }
        }

        def final expiredDateSubscriber = timeNow - 30 //TODO: make this a proper env var / constant
        log.info("expiring/deleting incomplete subscriber profiles that were not completed within the 30 days period as of " + expiredDateSubscriber)

        def final subscribersQuery = Subscriber.where {
            (verified == false && dateCreated < expiredDateSubscriber)
        }

        def final subscribers = subscribersQuery.list(sort:"email")
        log.info("number of unverified subscribers (for removal) found: " + subscribers.size())

        for (Subscriber subscriber : subscribers) {
            log.debug("deleting subscriber: " + subscriber.email + " (" + subscriber.dateCreated + ")")
            subscriber.delete(flush:true)
        }
    }
}
