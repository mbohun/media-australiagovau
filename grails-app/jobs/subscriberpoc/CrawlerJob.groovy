package subscriberpoc

import java.util.Date
import org.apache.commons.logging.LogFactory


class CrawlerJob {

    private static final log = LogFactory.getLog(this)
    def crawlerService
    def concurrent = false

    def group = "crawlServices"

    static triggers = {
        // NOTE: the trigger configuration is now:
        //       - specified in Config.groovy
        //       - loaded by BootStrap.groovy
        //       - can be adjusted/customized via the MEDIARELEASES_CRAWL_JOB_CRON_CONFIG env var
    }

    def execute() {
        log.info("Crawler has been fired: " + new Date())
        crawlerService.crawl()
    }
}
