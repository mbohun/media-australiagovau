package subscriberpoc

import grails.transaction.Transactional


class MediaReleaseException extends RuntimeException {
    String message
    MediaRelease mediaRelease
}

@Transactional
class MediaReleaseService {

    MediaRelease createMediaRelease(params) {
        def mediaRelease = MediaRelease.findByUrl(params.url)
        if (!mediaRelease) {
            def mediaReleaseInstance = new MediaRelease(
                    url: params.url,
                    title: params.title,
                    snippet: params.snippet,
                    dateCreated: new Date(),
                    isMediaRelease: null,
                    site: params.site,
                    hasBeenSent: false,
                    verified: false
            )

            if (mediaReleaseInstance.validate() && mediaReleaseInstance.save()) {
                return mediaReleaseInstance
            } else{
                return "ERROR IN VALIDATION AND/OR SAVE"
            }
        } else {
            throw new MediaReleaseException(
                    message: "Release already exists", mediaRelease: mediaRelease)
        }
    }
}


