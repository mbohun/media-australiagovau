package subscriberpoc

import com.google.common.base.Stopwatch
import edu.uci.ics.crawler4j.crawler.CrawlConfig
import edu.uci.ics.crawler4j.crawler.CrawlController
import edu.uci.ics.crawler4j.crawler.Page
import edu.uci.ics.crawler4j.crawler.WebCrawler
import edu.uci.ics.crawler4j.fetcher.PageFetcher
import edu.uci.ics.crawler4j.parser.HtmlParseData
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer
import edu.uci.ics.crawler4j.url.WebURL
import grails.transaction.Transactional
import org.apache.commons.logging.LogFactory
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import java.util.regex.Pattern
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;

import subscriberpoc.util.ShortURLCreator
import subscriberpoc.util.ShortURLCreatorBitly

@Transactional
class CrawlerService {

    private static final log = LogFactory.getLog(this)
    static final String SYSTEM_PROPERTY_FILE_SEPARATOR = System.getProperty("file.separator")
    static final String SYSTEM_PROPERTY_TMP_DIR = System.getProperty("java.io.tmpdir")

    // NOTE: - HashSet mem req: (32 * SIZE + 4 * CAPACITY)
    //       - make the cache synchronized just in case someone later decided to use the crawler in multi-threaded mode
    //
    // TODO: IMHO it is safer (far less error prone) to create the cache Set with a Comparator that will enforce
    //       the URL comparison to be case insensitive, as opposed to rely on all the involved parties calling
    //       url.toLowerCase()
    //       PLUS! there is another minor problem causing a cache bypass because of trailing "/",
    //       for example: "http://accc.gov.au/media/media-releases/" vs "http://accc.gov.au/media/media-releases", so
    //       the same Set/Comparator could enforce comparing URLs stripping the trailing "/"
    //
    static final Set cacheVisited = Collections.synchronizedSet(new HashSet());
    static long cacheVisitedMem = 4 * 16; // 4 * CAPACITY bytes; for the mem usage (HashSet): 32 * SIZE + 4 * CAPACITY bytes

    // TODO: this needs a review and cleanup: We load the filter from the DB ONLY once on the first crawl after the app
    //       startup, and then we just keep adding new URLs to it; most are going to be saved into the DB; the only scenario
    //       where this approach would fail is if someone kept the app/service running (so the cached URLs would stay in mem),
    //       disconnected/re-connected to a diff DB (that changed DB would have to use exactly the same credentials, settings
    //       like the original. HOWEVER even then such unlikely scenario would only waste some extra 3-4mb of mem (the existing
    //       cache filled with useless URL Strings from the previous sessions, and the first crawl after such DB swap would take
    //       longer (because there would be no positive lookups into the cache), HOWEVER that first crawl after the DB swap would
    //       add to the existing cache new URLs from the new DB, hence the subsequent crawls would be again using cache.
    //
    def crawl() {
        log.debug("Performing the crawl")
        final Stopwatch stopwatch = new Stopwatch().start()

        // TODO: 1. make the cache a proper class
        //       2. investigate what constraint can keep the list size as small as possible
        cacheVisited.clear()
        cacheVisitedMem = 4 * 16

        final def mediaReleasesAlreadyProcessed = MediaRelease.list()
        mediaReleasesAlreadyProcessed.each({ it ->
            final def url = it.url.toLowerCase()
            if (cacheVisited.add(url)) {

                // apx. per HashSet item mem usage
                cacheVisitedMem += 32 // 32 * SIZE + 4 * CAPACITY bytes; TODO: add CAPACITY bytes?

                // apx. String mem usage
                final int mem = 38 + (2 * url.length())
                final int padding = 8 - (mem % 8)

                cacheVisitedMem +=  mem + padding
            }
            //log.debug("shouldVisit:cacheVisited:items:" + cacheVisited.size() + "; size(" + cacheVisitedMem + "bytes); ADDED:" + url)
        })

        log.debug("cacheVisited:items:" + cacheVisited.size() + "; size(" + cacheVisitedMem + "bytes)")

        final def sites = Site.list(sort: "url", order: "asc")

        // format the list of sites into a human friendly list with on site per line
        def sitesFormatted = StringBuilder.newInstance()
        sitesFormatted << "[\n"

        sites.each({ it ->
            sitesFormatted << it << ",\n" })

        sitesFormatted << "]\n"

        log.debug(sitesFormatted)

        def noOfSites = sites.size()
        def count = 1;
        sites.each({ it ->
            doCrawl(it)
            // TODO: consider turning off *ALL* debug if NOT in debug mode
            def percent = (count/noOfSites * 100)
            def progress = new String(new char[percent]).replace('\0', '=') + ">" + new String(new char[100-percent]).replace('\0', ' ');
            log.debug("[" + progress + "] " + percent + "% complete in [" + stopwatch + "]" + " | " + count + "/" + noOfSites)
            count++
        })
        stopwatch.stop()
        log.debug(stopwatch)

    }

    private doCrawl(Site site) {
        log.debug("------------------------------------")
        log.debug("Crawling [" + site.url + "]")

        final String crawlStorageFolder = SYSTEM_PROPERTY_TMP_DIR + SYSTEM_PROPERTY_FILE_SEPARATOR + site.url.toURI().getHost() + SYSTEM_PROPERTY_FILE_SEPARATOR
        log.debug("crawlStorageFolder: " + crawlStorageFolder)

        final int numberOfCrawlers = 1;
        final CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);

        config.setPolitenessDelay(100);
        config.setMaxDepthOfCrawling(1);
        config.setMaxPagesToFetch(-1);

        config.setIncludeBinaryContentInCrawling(false);
        config.setResumableCrawling(false);

        final PageFetcher pageFetcher = new PageFetcher(config);
        final RobotstxtConfig robotsTxtConfig = new RobotstxtConfig();
        final RobotstxtServer robotsTxtServer = new RobotstxtServer(robotsTxtConfig, pageFetcher);
        final CrawlController controller = new CrawlController(config, pageFetcher, robotsTxtServer);

        controller.setCustomData(site)
        controller.addSeed(site.url)
        controller.start(CrawlerExtender.class, numberOfCrawlers);

        log.debug("------------------------------------")
    }
}

class CrawlerExtender extends WebCrawler {

    private static final Pattern FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g|png|tiff?|mid|mp2|mp3|mp4|wav|avi|mov|mpeg|ram|m4v|pdf|rm|smil|wmv|swf|wma|zip|rar|gz))\$")
    private static final log = LogFactory.getLog(this)
    private Site site

    // TODO: this should really be exposed in the MRS web GUI, and these links stored in the DB, so the app's admins can edit these interactively
    //
    static def FILTER_SOCIAL_MEDIA = [
        "https://accounts.google.com/ServiceLogin",
        "https://au.pinterest.com/join",
        "https://www.pinterest.com/join",
        "https://www.facebook.com/login",
        "https://www.linkedin.com/uas/login"
    ] as String[]

    static final String MRS_BITLY_ACCESS_TOKEN = System.getenv("MRS_BITLY_ACCESS_TOKEN")?: "Bitly access token not set!";
    static final ShortURLCreator shortUrlCreator = new ShortURLCreatorBitly(MRS_BITLY_ACCESS_TOKEN)

    @Override
    public void onStart() {
        site = myController.getCustomData()
        log.debug("onStart:" + site.releaseUrl)
    }

    @Override
    public boolean shouldVisit(Page page, WebURL url) {
        log.debug("shouldVisit:url=" + url + ", site=" + site.releaseUrl.toLowerCase())
        final String href = url.getURL().toLowerCase();

        if (CrawlerService.cacheVisited.contains(href)) {
            log.debug("shouldVisit:cacheVisited:ALREADY PROCESSED:" + href)
            return false;
        }

        // else {
        //     // NOTE: *EVERYTHING* that is not in the cache already, is being added here in order to:
        //     //        a) URLs that are going to be rejected bellow (png/jpg/gif/etc., social media, known URLs we do not want to follow/crawl)
        //     //           this is to avoid processing them again-and-again; so that is pure optimization.
        //     //        b) URLs that are going evaluate to true and they will be saved into the DB (NOTE: the save operation into DB rejects
        //     //           the non-unique ones (duplicates).
        //     //
        //     log.debug("shouldVisit:cacheVisited:ADD:" + href)
        //     CrawlerService.cacheVisited.add(href)
        // }

        // NOTE: original/untouched processing
        for (int i = 0; i < FILTER_SOCIAL_MEDIA.size(); i++) {
            // TODO: this is a possible BUG, because contains would match even in case where the social media URL is used as some param in the URL
            if (href.contains(FILTER_SOCIAL_MEDIA[i].toLowerCase())) {
                log.debug("shouldVisit: FILTER_SOCIAL_MEDIA:" + FILTER_SOCIAL_MEDIA[i] + ": false")
                // TODO: add to cache, HOWEVER that make sense only with 1 cache init per class load, not when building the cache from scratch
                //       at the beginning of each crawl; NOTE: perhaps a hybrid solution could be used to collect/keep all the these
                //       false-filters rejected URLs into a separate cache, and them merge those 2 caches at the beginning of each crawl?
                return false;
            }
        }

        if (FILTERS.matcher(href).matches()) {
            log.debug("shouldVisit: FILTERS.matcher: false")
            return false;
        }

        // TODO: verify if this can follow directly after the cache check? OR if it relies on social media, regexp filters to be
        //       applied first?
        //
        if (href.contains(site.releaseUrl.toLowerCase())) {
            log.debug("shouldVisit: href.contains: true")
            return true;
        }

        log.debug("shouldVisit: false (fallen through)")
        return false;
    }

    @Override
    public void visit(Page page) {
        final String url = page.getWebURL().getURL();
        log.debug("visit:" + url)

        if (CrawlerService.cacheVisited.contains(url)) {
            log.debug("visit:cacheVisited:ALREADY PROCESSED:" + url)
            return;

        } else {
            if (CrawlerService.cacheVisited.add(url)) {

                // apx. per HashSet item mem usage
                CrawlerService.cacheVisitedMem += 32 // 32 * SIZE + 4 * CAPACITY bytes; TODO: add CAPACITY bytes?

                // apx. String mem usage
                final int mem = 38 + (2 * url.length())
                final int padding = 8 - (mem % 8)

                CrawlerService.cacheVisitedMem +=  mem + padding
                log.debug("visit:cacheVisited:ADD:" + url)
            }      
        }

        // TODO: final HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
        //       do it once then re-use it
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String html = htmlParseData.getHtml();
            Document doc = Jsoup.parse(html);
            boolean descriptionFound = false;
            def descriptionElements;
            String description = getMetaTag(doc, "description");
            if(description != null) {
                descriptionFound = true;
            } else {
                description = getMetaTag(doc, "DC:Description");
                if(description != null) {
                    descriptionFound = true;
                } else {
                    description = getMetaTag(doc, "DCTerms:Description");
                    if(description != null) {
                        descriptionFound = true;
                    }
                }
            }
            if(!descriptionFound) {
                descriptionElements = doc.select("p");
                if (descriptionElements != null && !descriptionElements.isEmpty()) {
                    descriptionElements.each({ it ->
                        if (!descriptionFound) {
                            description = it.text();
                            if (description.length() > 125 && !description.toLowerCase().contains('javascript')) {
                                description = description.substring(0, Math.min(description.length(), 200)) + "...";
                                descriptionFound = true;
                                return true
                            }
                        }
                    })
                }
            }
            if(!descriptionFound) {
                //No suitable description found for P tags, now lets try divs
                descriptionElements = doc.select("div");
                if (descriptionElements != null && !descriptionElements.isEmpty()) {
                    descriptionElements.each({ it ->
                        if(!descriptionFound) {
                            description = it.text();
                            if (description.length() > 125 && !description.toLowerCase().contains('javascript')) {
                                description = description.substring(0, Math.min(description.length(), 200)) + "...";
                                descriptionFound = true;
                                return true
                            }
                        }
                    })
                }
            }
            if(!descriptionFound) {
                log.debug("No suitable description found for [" + url + "] try setting a selector and we can implement that at some point")
            }

            // TODO: add support for a custom URL sanitizer here.
            //
            if (url.length() > 255) {
                url = url.substring(0, url.indexOf('?'))
                log.debug("sanitized URL: " + url)
            }

            def releaseDetails = new MediaRelease(site: site, title: htmlParseData.getTitle(), url: url, shortUrl: shortUrlCreator.create(url), snippet: description)

            // NOTE: in this crawler cache version *ONLY* really new media releases should made it up to this point
            MediaRelease.withTransaction { txStatus ->
                if(releaseDetails.save()) {
                    log.debug(releaseDetails.url + " \u2713")
                } else {
                    log.debug(releaseDetails.url + " \u2A2F")
                }
            }
        }
//Elements mediaReleaseSelector = doc.select(mediaReleaseSelector)
    }


    String getMetaTag(Document document, String attr) {
        Elements elements = document.select("meta[name=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null && !s.equals("")) return s;
        }
        elements = document.select("meta[property=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null && !s.equals("")) return s;
        }
        return null;
    }

}
