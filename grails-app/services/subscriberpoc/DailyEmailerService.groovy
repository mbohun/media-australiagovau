package subscriberpoc
import grails.transaction.Transactional
import grails.util.Holders
import groovy.xml.MarkupBuilder
import org.apache.commons.logging.LogFactory
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

import java.text.SimpleDateFormat

@Transactional
class DailyEmailerService {


    private static final log = LogFactory.getLog(this)
    def mailService
    def LinkGenerator grailsLinkGenerator

    def send(List releases) {
        if(releases != null && !releases.isEmpty()) {
            def newReleases = MediaRelease.findAllByIdInList(releases)

            if (!newReleases.isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy")
                def theDate = sdf.format(new Date())

                //NOTE: hidden env var MEDIARELEASES_SEND_RESUME_FROM
                def resumeFromEmail = System.getenv("MEDIARELEASES_SEND_RESUME_FROM")?: "0"
                log.debug("resumeFromEmail: " + resumeFromEmail)

                def subscribersQuery = Subscriber.where {
                    (verified == true && email >= resumeFromEmail)
                }

                def subscribers = subscribersQuery.list(sort:"email")
                log.debug("number of subscribers found: " + subscribers.size())

                for (Subscriber subscriber : subscribers) {
                    log.debug("subscriber: " + subscriber.email)
                }

                for (Subscriber subscriber : subscribers) {
                    def releasesToBeMailed = []
                    for (MediaRelease release : newReleases) {
                        def addedToMailList = false;
                        subscriber.topics.each({ it ->
                            if (release.site.topics.contains(it) && !addedToMailList) {
                                releasesToBeMailed.add(release)
                                addedToMailList = true;
                            }

                        })
                    }
                    //TODO: move the email/HTML creation out of here; create/prepare emails first and then pass them in here for emailing/distribution only
                    if (!releasesToBeMailed.isEmpty()) {
                        releasesToBeMailed.sort { a,b -> a.title <=> b.title };
                        def writer = new StringWriter()  // html is written here by markup builder
                        def markup = new MarkupBuilder(writer)  // the builder
                        markup.html {
                            head {
                                title"Australian Government Media Release Service"
                                style(type:'text/css', "body{padding:40px;font-family: \"Open Sans\",\"Lucida Sans\",Arial,Helvetica,sans-serif !important;}h1{color:#ca3632;font-weight:600;font-size:20px;}h2{color: #00002d;margin-bottom: 0.3em;margin-top: 1.5em;font-size:14px;}p.url{color: #16589c;}p.snippet{}")
                            }
                            body {
                                h1"Australian Government Media Release Service"
                                div"These media releases were found on " + theDate + " in response to your request for new media releases"
                                p {
                                    mkp.yieldUnescaped("<br />")
                                    p {
                                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'modify', id: subscriber.confirmCode) + "?email=" + subscriber.email, "Modify your Subscription")
                                    }
                                    p {
                                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'remove', id: subscriber.confirmCode) + "?email=" + subscriber.email, "Cancel your Subscription")
                                    }
                                    p {
                                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'index', id: subscriber.confirmCode), "Create a new Subscription")
                                    }
                                }
                                div {
                                    releasesToBeMailed.each({ rel ->
                                        h2(rel.title)
                                        p(class: "url") {
                                            a(href:rel.shortUrl ? rel.shortUrl : rel.url, rel.url)
                                        }
                                        if(rel.snippet != null) {
                                            p(class: "snippet", rel.snippet)
                                        }
                                    })
                                }
                                p {
                                    mkp.yieldUnescaped("<br />")
                                    mkp.yieldUnescaped("<br />")
                                    p {
                                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'modify', id: subscriber.confirmCode) + "?email=" + subscriber.email, "Modify your Subscription")
                                    }
                                    p {
                                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'remove', id: subscriber.confirmCode) + "?email=" + subscriber.email, "Cancel your Subscription")
                                    }
                                    p {
                                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'index', id: subscriber.confirmCode), "Create a new Subscription")
                                    }
                                }
                            }
                        }

                        try {
                            mailService.sendMail{
                                to subscriber.email
                                subject "Australian Government Media Releases for " + theDate + " [SEC=UNCLASSIFIED]"
                                from Holders.config.mail.from
                                html writer.toString()
                            }

                        } catch (Exception e) {
                            log.error("Sent email to [" + subscriber.email + "] FAILED: " + e.getMessage())
                            e.printStackTrace()
                            continue
                        }

                        log.debug("Sent email to [" + subscriber.email + "] with [" + releasesToBeMailed.size() + "] releases.")
                    } else {
                        log.debug("No email to send to subscriber [" + subscriber.email + "] today.")
                    }
                }
                //TODO: this will need more analysis/testing
                for (MediaRelease release : newReleases) {
                    release.hasBeenSent = true;
                    release.save();
                }

            } else {
                log.debug("**** No releases captured to send today ****")
            }

        }
    }
}
