package subscriberpoc

import grails.transaction.Transactional

class SubscriberException extends RuntimeException {
    String message
    Subscriber subscriber
}

@Transactional
class SubscriberService {

    Subscriber createSubscriber(params) {
        def subscriber = Subscriber.findByEmail(params.email)
        if (!subscriber) {
            def subscriberInstance = new Subscriber(
                    email: params.email,
                    topics: params.topics,
                    verified: false,
                    accepted: params.accepted,
                    confirmCode: UUID.randomUUID().toString()
            )
            if(subscriberInstance.validate()&& subscriberInstance.save()){


                return subscriberInstance
            }
        } else {
            throw new SubscriberException(
                    message: "Subscription exists for this email address", subscriber: subscriber)

        }
    }



}


