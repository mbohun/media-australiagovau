package subscriberpoc

import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.ui.RegistrationCode
import grails.transaction.Transactional
import groovy.xml.MarkupBuilder
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

@Transactional
class RegisterService {

    def mailService
    def LinkGenerator grailsLinkGenerator

    def sendRegEmail(params) {
        def user = User.findByEmail(params)

        RegistrationCode registrationCode = new RegistrationCode(username: user.username)

        registrationCode.save(flush: true)

        def writer = new StringWriter()
        def markup = new MarkupBuilder(writer)
        markup.html {
            head {
                title "Australian Government Media Release Service"
                style(type:'text/css', "body{padding:40px;font-family: \"Open Sans\",\"Lucida Sans\",Arial,Helvetica,sans-serif !important;}h1{color:#ca3632;font-weight:600;font-size:20px;}h2{color: #00002d;margin-bottom: 0.3em;margin-top: 1.5em;font-size:14px;}p.url{color: #16589c;}p.snippet{}")
            }
            body {
                h2 "Australian Government Media Release Registration"
                div "An account with this email address has been created for you in the media release system."
                p {
                    a(href: grailsLinkGenerator.link(absolute: true, controller: 'register', action: 'resetPassword') + "?t=" + registrationCode.token, "Please click here to finish the registration")
                }
            }
        }




        def conf = SpringSecurityUtils.securityConfig


        mailService.sendMail {
            to user.email
            from conf.ui.forgotPassword.emailFrom
            subject conf.ui.forgotPassword.emailSubject
            html writer.toString()
        }

        [emailSent: true]
    }


}
