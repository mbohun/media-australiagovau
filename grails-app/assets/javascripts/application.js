// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require_tree .
//= require_self
//= require bootstrap

if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});

		$(document).ready(function() {
			//Select All on Subscribe Create page
			if($('div#create-subscriber, div#edit-subscriber').length) {
				$('#selectAllCheckBox').change(function () {
					var isChecked = $(this).is(":checked");
					$("[name='topics']").each(function () {
						$(this).prop("checked", isChecked);
					});
				});

				$("[name='topics']").each(function () {
					$(this).click(function () {
						$('#selectAllCheckBox').prop("checked", false);
						var isChecked = $(this).is(":checked");
						if(isChecked){
							$(this).is(":unchecked");
						}
					});
				});
			}

			if($('div#list-mediaRelease').length) {
				$('#selectAllApprove').click(function () {
					if($(this).html() == "Select All") {
						$(this).html("Deselect All");
						$("[name='release']").each(function () {
							$(this).prop("checked", true);
						});
					} else {
						$(this).html("Select All");
						$("[name='release']").each(function () {
							$(this).prop("checked", false);
						});
					}
				});
			}

//			if($('.logged-in').length) {
//				window.setTimeout(function() {
//					$('#signOut').click();
//				}, 900000)
//			}

			jQuery(window).load(function() {
				equalheight('div.container div.row label.subscriberCreateBox');
			})

			jQuery(window).resize(function() {
				equalheight('div.container div.row label.subscriberCreateBox');
			})
		});


		equalheight = function(container){

			var currentTallest = 0,
				currentRowStart = 0,
				rowDivs = new Array(),
				el,
				topPosition = 0;
			jQuery(container).each(function() {

				el = jQuery(this);
				jQuery(el).height('auto')
				topPostion = el.position().top;

				if (currentRowStart != topPostion) {
					for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
						rowDivs[currentDiv].height(currentTallest);
					}
					rowDivs.length = 0; // empty the array
					currentRowStart = topPostion;
					currentTallest = el.height();
					rowDivs.push(el);
				} else {
					rowDivs.push(el);
					currentTallest = (currentTallest < el.height()) ? (el.height()) : (currentTallest);
				}
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}
			});
		}
	})(jQuery);
}

function showHide(id) {
	var descriptionSelector = '#description' + id;
	if(jQuery(descriptionSelector).hasClass('hide')) {
		jQuery(descriptionSelector).removeClass('hide');
	} else {
		jQuery(descriptionSelector).addClass('hide');
	}

}

