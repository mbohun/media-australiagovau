import com.google.common.base.Stopwatch
import edu.uci.ics.crawler4j.crawler.CrawlConfig
import edu.uci.ics.crawler4j.crawler.CrawlController
import edu.uci.ics.crawler4j.crawler.Page
import edu.uci.ics.crawler4j.crawler.WebCrawler
import edu.uci.ics.crawler4j.fetcher.PageFetcher
import edu.uci.ics.crawler4j.parser.HtmlParseData
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer
import edu.uci.ics.crawler4j.url.WebURL
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements

import javax.mail.Message
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import java.util.regex.Pattern

String user = "Pablo"
String pass = "password"

String host = "localhost"
String port = "1025"
String from = "noreply@localhost";

Properties properties = System.getProperties();
properties.setProperty("mail.smtp.host", host);
properties.setProperty("mail.smtp.port", port)
properties.put("mail.debug", "false");

Session session = Session.getInstance(properties);
String base_url = "http://localhost:8080/SubscriberPOC/";

println("Running Media Release Crawler")

def http_base = new HTTPBuilder(base_url)
def foundReleases = []
def releasesAdded = []

def cookies = []

//Authenticate
http_base.request(Method.POST) {
    uri.path = 'j_spring_security_check'
    requestContentType = ContentType.URLENC
    body = [j_username: user, j_password: pass]
    response.'302' = { resp, reader ->
        print("Response: " + resp.statusLine.toString())
        println("Authenticated")
        resp.getHeaders('Set-Cookie').each {
            String cookie = it.value.split(';')[0]
            cookies.add(cookie)
        }
    }
    response.failure = { resp ->
        println("Unexpected error: ${resp}")
    }
}
println("Performing the crawl")
Stopwatch stopwatch = new Stopwatch().start()



def sites = getListOfSites(base_url)

/**
 * Do the crawl and create mediaRelease objects
 */

for (Map site : sites) {

    println "Starting Crawl on: " + site['url']

    String crawlStorageFolder = "/tmp/" + site.url.toURI().getHost() + "/"

    int numberOfCrawlers = 1;
    CrawlConfig config = new CrawlConfig();
    config.setCrawlStorageFolder(crawlStorageFolder);

    config.setPolitenessDelay(1000);
    config.setMaxDepthOfCrawling(-1);
    //TODO: Change this to -1 when proper testing
    config.setMaxPagesToFetch(-1);

    config.setIncludeBinaryContentInCrawling(false);
    config.setResumableCrawling(false);

    PageFetcher pageFetcher = new PageFetcher(config);
    RobotstxtConfig robotsTxtConfig = new RobotstxtConfig();
    RobotstxtServer robotsTxtServer = new RobotstxtServer(robotsTxtConfig, pageFetcher);
    CrawlController controller = new CrawlController(config, pageFetcher, robotsTxtServer);
    String[] customData = new String[7];
    customData[0] = site['url']
    customData[1] = site['created']
    customData[2] = site['description']
    customData[3] = site['agency']['title']
    customData[4] = site['createdRegex']
    customData[5] = site['mediaReleaseSelector']
    customData[6] = site['releaseUrl']

    controller.setCustomData(customData)
    controller.addSeed(site['url']);
    controller.start(CrawlerExtender.class, numberOfCrawlers);

    mapReleases = controller.getCustomData();


    for (Map release : mapReleases) {
        release['site'] = site
        foundReleases.add(release)
    }

    println "Finished Crawl on: " + site.url
    println "Number of Media Releases Found: " + mapReleases.size()

}

// stop timer and print stats
stopwatch.stop()

println "Complete Crawl finished in: " + stopwatch
println "Total number of new Media Releases Found: " + foundReleases.size()


for (Map release : foundReleases) {
    println "Performing POST on: mediaRelease..."
    def httpSendRelease = new HTTPBuilder(base_url + 'api/mediareleases')

    httpSendRelease.request(Method.POST, ContentType.JSON) { req ->
        headers.Cookie = cookies.join(';')

        body = release

        response.success = { resp ->

            print("Response: " + resp.statusLine.toString())
            releasesAdded.add(release)
        }
        response.failure = { resp ->
            println("Release [" + release['title'] + "] already exists")
        }
    }
}


if (!releasesAdded.isEmpty()) {
    //println("Sending test email with [" + releasesAdded.size() + "] media releases")

    def subscribers = getListOfVerifiedSubscribers(base_url)    // this is a list of maps representing subscriber data
   // println "There are " + subscribers.size() + " subscribers"


    for (Map subscriber : getListOfVerifiedSubscribers(base_url)) {
        //println("Checking user " + subscriber['email'])
        def releasesToBeMailed = []

        //check if any of the new releases are part of user's subscription
        if (releasesAdded) {
            for (Map release : releasesAdded) {

                def releaseTopics = release['site']['topics']
                println releaseTopics
                subscriber['topics'].each({ it ->
                    println it
                    if (releaseTopics.contains(it)) {
                        println "this one is one of the releases"
                        println it
                        releasesToBeMailed.add(release)
                    }
                })
            }

            if (!releasesToBeMailed.isEmpty()) {
                //println("Sending email to [" + subscriber['email'] + "]")

//                String releaseString = "<b>Media Releases</b>"
                String releaseString = """Media Releases
Your subscriptions"""
//                releaseString += "<h2>Your Subscriptions</h2>"
//                releaseString += "<ul>"
                releasesToBeMailed.each({ it ->

                    releaseString += """${it['title']}
${it['url']}
${it['snippet']}


"""

                })

                releaseString += """To modify your subscription click here
Or to unsubscribe click here
"""
//                releaseString += "</ul>"
//                releaseString += "<p>To modify your subscription click here</p>"
//                releaseString += "<p>To unsubscribe click here</p>"


                String to = subscriber['email']
               // println "Email will go to: " + to
              //  println "-----------------"
              //  println releaseString
                MimeMessage message = new MimeMessage(session);

                message.setFrom(new InternetAddress(from));
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject("Media Releases");
                message.setText(releaseString, "utf-8", "html");

                Transport.send(message);

            } else {
                println("No new releases for this subscriber")
            }
        } else {
            println("No new releases were found")
        }
    }
}


private getListOfVerifiedSubscribers(String base_url) {
    def verifiedSubscribers
    println "Performing GET on: subscribers..."

    def httpGetSubscribers = new HTTPBuilder(base_url + 'api/subscribers')
    httpGetSubscribers.request(Method.GET) { req ->
        headers.Accept = 'application/json'

        response.success = { resp, reader ->

            verifiedSubscribers = reader

            return verifiedSubscribers
        }
        response.failure = { resp ->
            println("Unexpected error when retrieving a list of subscribers: ${resp}")
        }
    }
    return verifiedSubscribers
}


private getListOfSites(String base_url) {
    /**
     * get the list of sites
     */
    println "Performing GET on: sites..."
    def sitesFound
    def httpGetSites = new HTTPBuilder(base_url + 'api/sites')

    httpGetSites.request(Method.GET) { req ->
        headers.Accept = 'application/json'

        response.success = { resp, reader ->
            sitesFound = reader

        }
        response.failure = { resp ->
            println("Unexpected error when retrieving a list of sites: ${resp}")
        }
    }
    return sitesFound
}


class CrawlerExtenderX extends WebCrawler {

    private static
    final Pattern FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g|png|tiff?|mid|mp2|mp3|mp4|wav|avi|mov|mpeg|ram|m4v|pdf|rm|smil|wmv|swf|wma|zip|rar|gz))\$")

    private String myCrawlDomains;
    private String createdMeta;
    private String descriptionMeta;
    private String agency;
    private String createRegex;
    private String mediaReleaseSelector;
    private String releaseUrl;


    @Override
    public void onStart() {
        String[] customData = (String[]) myController.getCustomData();
        myCrawlDomains = customData[0];
        createdMeta = customData[1];
        descriptionMeta = customData[2];
        agency = customData[3];
        createRegex = customData[4];
        mediaReleaseSelector = customData[5];
        releaseUrl = customData[6];
        myController.setCustomData(new ArrayList<String>(0));
    }

    @Override
    public boolean shouldVisit(Page page, WebURL url) {
        String href = url.getURL().toLowerCase();
        if (FILTERS.matcher(href).matches()) {
            return false;
        }

        if (href.startsWith(releaseUrl)) {
            return true;
        }
        return false;
    }

    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        println "URL: [" + url + "]"

        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String html = htmlParseData.getHtml();

            Document doc = Jsoup.parse(html);
            if (mediaReleaseSelector == null) {
                println "No MediaRelease Selector Found Adding all Pages"
                Elements descriptionElements;
                Elements createdElements;
                String description = "";
                Date createdDate = new Date();
                if (descriptionMeta != null) {
                    descriptionElements = doc.select("meta[name=" + descriptionMeta + "]");
                    if (descriptionElements != null && !descriptionElements.isEmpty()) {
                        description = descriptionElements.get(0).attr("content");
                    }
                }
                if (createdMeta != null) {
                    createdElements = doc.select("meta[name=" + createdMeta + "]")
                    if (createdElements != null && !createdElements.isEmpty()) {
                        String created = createdElements.get(0).attr("content");
                        createdDate = Date.parse(createRegex, created)
                    }
                }

                def releaseDetails = [title: htmlParseData.getTitle(), snippet: description, url: page.getWebURL().getURL(), releaseDate: createdDate, isMediaRelease: false]

                myController.getCustomData().add(releaseDetails)
                println("Media Release found with no create date")
            } else {
                Elements mediaReleaseSelector = doc.select(mediaReleaseSelector)
                if (mediaReleaseSelector == null || mediaReleaseSelector.isEmpty()) {
                    println("Not a media release")
                    println("=============");
                    return
                } else {
                    Elements descriptionElements;
                    Elements createdElements;
                    String description = "";
                    Date createdDate = new Date();
                    if (descriptionMeta != null) {
                        descriptionElements = doc.select("meta[name=" + descriptionMeta + "]");
                        if (descriptionElements != null && !descriptionElements.isEmpty()) {
                            description = descriptionElements.get(0).attr("content");
                        }
                    }
                    if (createdMeta != null) {
                        createdElements = doc.select("meta[name=" + createdMeta + "]")
                        if (createdElements != null && !createdElements.isEmpty()) {
                            String created = createdElements.get(0).attr("content");
                            createdDate = Date.parse(createRegex, created)
                        }
                    }

                    def releaseDetails = [title: htmlParseData.getTitle(), snippet: description, url: page.getWebURL().getURL(), releaseDate: createdDate]

                    myController.getCustomData().add(releaseDetails)
                    println("Media Release found")

                }
            }
        }
        println("=============");
    }
}

