package subscriberpoc

import grails.converters.JSON

class TopicRestController {

    static responseFormats = ['json','xml']


    def index(String v) {
         println "Inside topicrestcontroller"
        def configName = 'v' + (v ?:1)

        JSON.use(configName){
            respond Topic.all
        }

    }
}
