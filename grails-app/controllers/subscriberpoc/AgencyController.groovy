package subscriberpoc

import grails.plugin.springsecurity.annotation.Secured
import org.apache.commons.logging.LogFactory

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN'])
class AgencyController {

    private static final log = LogFactory.getLog(this)
    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Agency.list(params), model: [agencyInstanceCount: Agency.count()]
    }


    def show(Agency agencyInstance) {
        respond agencyInstance
    }

    def create() {
        respond new Agency(params)
    }

    @Transactional
    def save(Agency agencyInstance) {
        if (agencyInstance == null) {
            notFound()
            return
        }

        if (agencyInstance.hasErrors()) {
            respond agencyInstance.errors, view: 'create'
            return
        }

        log.debug("User [" + springSecurityService.principal.username + "] creating agency [" + agencyInstance.title + "]")
        agencyInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'agency.label', default: 'Agency'), agencyInstance.id])
                redirect agencyInstance
            }
            '*' { respond agencyInstance, [status: CREATED] }
        }
    }

    def edit(Agency agencyInstance) {
        respond agencyInstance
    }

    @Transactional
    def update(Agency agencyInstance) {
        if (agencyInstance == null) {
            notFound()
            return
        }

        if (agencyInstance.hasErrors()) {
            respond agencyInstance.errors, view: 'edit'
            return
        }

        log.debug("User [" + springSecurityService.principal.username + "] updating agency [" + agencyInstance.title + "]")
        agencyInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Agency.label', default: 'Agency'), agencyInstance.id])
                redirect agencyInstance
            }
            '*' { respond agencyInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Agency agencyInstance) {

        if (agencyInstance == null) {
            notFound()
            return
        }

        log.debug("User [" + springSecurityService.principal.username + "] deleting agency [" + agencyInstance.title + "]")
        agencyInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Agency.label', default: 'Agency'), agencyInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'agency.label', default: 'Agency'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
