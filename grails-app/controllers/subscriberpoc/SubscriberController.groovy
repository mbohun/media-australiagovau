package subscriberpoc

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import grails.util.Holders
import groovy.xml.MarkupBuilder
import org.apache.commons.logging.LogFactory
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

@Transactional(readOnly = false)
class SubscriberController {


    private static final log = LogFactory.getLog(this)
    def mailService
    def LinkGenerator grailsLinkGenerator
    def springSecurityService
    def subscriberService


    def index() {
        log.debug("index, params:" + params)
        [subscriberInstance: new Subscriber(params)]

    }

    @Secured(['ROLE_ADMIN'])
    def list(Integer max) {
        log.debug("list")
        def subscribers = []
        params.max = (max == null) ? 500 : max
        params.sort = (params.sort == null) ? "email" : params.sort
        log.debug("params: " + params)
        subscribers = Subscriber.list(params)


        respond subscribers

    }

    def cancel() {
        log.debug("cancel")
        redirect(action: 'index')
    }


    def signup() {
        log.debug("signup")
        withForm {
            def subscriberInstance
            try {
                log.debug(params)
                def existingUser = Subscriber.findByEmail(params.email)
                if (existingUser != null) {
                    log.debug("User already exists")
                    if (!existingUser.verified) {
                        log.debug("User already exists and not verified")
                        existingUser.delete()
                        subscriberInstance = subscriberService.createSubscriber(params)
                    } else {
                        flash.message = "User already exists."
                        //redirect(action: "index", model: [subscriberInstance: subscriberInstance])
                    }
                } else {
                    subscriberInstance = subscriberService.createSubscriber(params)
                }


            } catch (SubscriberException se) {
                flash.message = se.message
                redirect(action: 'index')
            }

            if (subscriberInstance != null) {
                mailService.sendMail {
                    to subscriberInstance.email
                    subject "New Subscription Confirmation"
                    from Holders.config.mail.from
                    html g.render(template: "mailtemplate", model: [code: subscriberInstance.confirmCode, email: subscriberInstance.email])
                }

                render(view: "success", model: [subscriberInstance: subscriberInstance])
                redirect(action: "success")
            } else {
                if (params.accepted == null) {
                    flash.message = 'Please ensure you accept the Terms and Conditions.'
                } else if (params.topics == null) {
                    flash.message = 'Please ensure you select at least one Topic.'
                } else {
                    //flash.message = 'Please fix the errors below.'
                }
                redirect(action: "index", model: [subscriberInstance: subscriberInstance])
            }
        }.invalidToken {
            // bad request
            flash.message = 'Invalid Request'
            redirect(action: 'index')
        }
    }

    def success() {
        log.debug("success")
        flash.message = 'Your subscription is almost created. Please complete the process ' +
                'using the email we have now sent to your email address'
        render(view: 'index')


    }

    def confirm(String id, String email) {
        log.debug("confirm")
        Subscriber subscriberInstance = Subscriber.findByConfirmCode(id)
        if (!subscriberInstance) {
            render(view: "success", model: [message: 'Problem confirming account'])
            return
        }

        subscriberInstance.verified = true
        if (!subscriberInstance.save(flush: true)|| !subscriberInstance.email.equals(email)) {
            render(view: "success", model: [message: 'Problem activating account'])
            return
        }

        def writer = new StringWriter()  // html is written here by markup builder
        def markup = new MarkupBuilder(writer)  // the builder
        markup.html {
            head {
                title"Australian Government Media Release Service"
                style(type:'text/css', "body{padding:40px;font-family: \"Open Sans\",\"Lucida Sans\",Arial,Helvetica,sans-serif !important;}h1{color:#ca3632;font-weight:600;font-size:20px;}h2{color: #00002d;margin-bottom: 0.3em;margin-top: 1.5em;font-size:14px;}p.url{color: #16589c;}p.snippet{}")
            }
            body {
                h1"Australian Government Media Release Service"
                div"Thank you for subscribing to Media Release notifications."
                p {
                    mkp.yieldUnescaped("<br />")
                    p {
                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'modify', id: subscriberInstance.confirmCode) + "?email=" + subscriberInstance.email, "Modify your Subscription")
                    }
                    p {
                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'remove', id: subscriberInstance.confirmCode) + "?email=" + subscriberInstance.email, "Cancel your Subscription")
                    }
                    p {
                        a(href: grailsLinkGenerator.link(absolute: true, controller: 'subscriber', action: 'index'), "Create a new Subscription")
                    }
                }
                div {
                    h2"Your details are:"
                    p"Email: " + subscriberInstance.email
                }
                div {
                    p"You may view all the latest Media Releases by visiting "
                    a(href:grailsLinkGenerator.link(absolute: true, controller: 'mediaRelease', action: 'index'), grailsLinkGenerator.link(absolute: true, controller: 'mediaRelease', action: 'index'))
                    p""
                }
                div {
                    p {
                        mkp.yield "If you experience any difficulty or require assistance, please contact us at "
                        a(href: 'http://australia.gov.au/contact-us', "http://australia.gov.au/contact-us")
                        mkp.yield "."
                    }
                    p"The Government Media Release Service is provided by the Digital Transformation Agency. Please note that this service does not monitor the content of media releases, which remain the responsibility of the relevant authoring agency."
                    p"This media release service is brought to you by the Digital Transformation Agency through Australia.gov.au"
                }
            }
        }

        mailService.sendMail{
            to subscriberInstance.email
            from Holders.config.mail.from
            subject "Australian Government Media Releases [SEC=UNCLASSIFIED]"
            html writer.toString()
        }

        flash.message = 'Your subscription has been successfully activated'
        render(view: 'index')
    }

     def modify(String id, String email) {
         log.debug("modify")
             Subscriber subscriberInstance = Subscriber.findByConfirmCode(id)
             if (!subscriberInstance || !subscriberInstance.email.equals(email)) {
                 render(view: "success", model: [message: 'Problem accessing account'])
                 return
             }
             render(view: "manage_subscriber", model: [subscriberInstance: subscriberInstance, editing: true])
    }
    @Transactional
    def update(String id) {
        log.debug("update:id=" + id)

        Subscriber subscriberInstance = Subscriber.findByConfirmCode(id)
        log.debug("update:" + subscriberInstance)

        if (!subscriberInstance) {
          render(view: "success", model: [message: 'Problem accessing account'])
          return
        }

        withForm {
            def topicsById = Topic.getAll(params.get('topics'))
            subscriberInstance.topics = topicsById
            if (subscriberInstance == null) {
                notFound()
                return
            }

            if (subscriberInstance.isDirty('email')) {
                log.debug("User somehow tried to change their email, resetting it back to original value")
                subscriberInstance.email = subscriberInstance.getPersistentValue('email')
            }

            if (subscriberInstance.isDirty('accepted')) {
                log.debug("User somehow tried to change their accepted, resetting it back to original value")
                subscriberInstance.accepted = subscriberInstance.getPersistentValue('accepted')
            }

            if (subscriberInstance.hasErrors()) {
                respond subscriberInstance.errors, view: 'edit'
                return
            }

            subscriberInstance.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Subscriber.label', default: 'Subscriber'), subscriberInstance.confirmCode])
                    redirect(action: 'showSubscriber', id: subscriberInstance.confirmCode, params: [email: subscriberInstance.email])

                }
                '*' { respond subscriberInstance, [status: OK] }

            }
        }.invalidToken {
            // bad request
            flash.message = 'Invalid Request'
            redirect(action: 'index')
        }
    }

    @Secured(['ROLE_ADMIN'])
    def show(Subscriber subscriberInstance) {
        log.debug("show")
        respond subscriberInstance
    }

    def showSubscriber(String id, String email){
        log.debug("showSubscriber")
        Subscriber subscriberInstance = Subscriber.findByConfirmCode(id)
        if (subscriberInstance == null) {
            render(view: "success", model: [message: 'Problem accessing account'])
            return
        }
        if (!subscriberInstance.email.equals(email)) {
            render(view: "success", model: [message: 'Problem accessing account'])
            return
        }
        render(view: "subscriber_show", model: [subscriberInstance: subscriberInstance])
    }

    def remove(String id, String email) {
        log.debug("remove")
        Subscriber subscriberInstance = Subscriber.findByConfirmCode(id)
        if (!subscriberInstance||!subscriberInstance.email.equals(email)) {
            render(view: "success", model: [message: 'Problem accessing account'])
            return
        }

        subscriberInstance.delete flush: true

        request.withFormat {

            form multipartForm {
                flash.message = message(message: 'You have cancelled your subscription.', args: [message(code: 'Subscriber.label', default: 'Subscriber'), subscriberInstance.confirmCode])
                render(view: 'index')
            }
        }
        render(view: 'index')
    }

    @Secured(['ROLE_ADMIN'])
    @Transactional
    def delete(Subscriber subscriberInstance) {
        log.debug("delete")
        if (subscriberInstance == null) {
            notFound()
            return
        }

        log.debug("User [" + springSecurityService.principal.username + "] deleting subscriber [" + subscriberInstance.email + "]")
        subscriberInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Subscriber.label', default: 'Subscriber'), subscriberInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def edit(Subscriber subscriberInstance) {
        log.debug("edit")
        render(view: "edit", model: [subscriberInstance: subscriberInstance, editing: true])
        //respond subscriberInstance
    }


}