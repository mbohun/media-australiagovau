package subscriberpoc

import grails.util.Holders

class SubscriberRestController  {

    static responseFormats = ['json','xml']

    def subscriberService
    def mailService


    // there is no use case for api call to see all subscribers
//    def index(String v){
//        println "Inside subscriberrestcontroller"
//        def configName = 'v' + (v ?: 1)
//
//        JSON.use(configName){
//            respond Subscriber.findAllWhere(verified: true)
//        }
//
//    }



    def save(Subscriber subscriber){
        if(!subscriber.hasErrors()){
            def subscriberInstance = subscriberService.createSubscriber(subscriber)
            mailService.sendMail {
                to subscriberInstance.email
                from Holders.config.mail.from
                subject "New Subscription Confirmation"
                body(
                view:"/subscriber/_mailtemplate",
                model:[code: subscriberInstance.confirmCode, email:subscriberInstance.email])

            }
            respond subscriberInstance, status: 201
        }
        else{
            respond subscriber
        }

    }



}



