package subscriberpoc

import grails.util.Environment

class MediaReleaseRestController {

    static responseFormats = ['json','xml']

//    def mediaReleaseService

    def index() {
        if ((Environment.getCurrent() == Environment.DEVELOPMENT) || (Environment.getCurrent() == Environment.TEST)) {
            respond MediaRelease.findAll([sort:"dateCreated", order:"desc", max: 50, offset: params.offset]) {
            }

        } else {
            //NOTE: At this stage we are not modifying the Environment.PRODUCTION until we add support for authentication
            //      and/or some protection for non-authenticated requests if needed (for example we could add rate-limit
            //      on the number of unauth request github REST API style (https://developer.github.com/v3/search/#rate-limit)
            //
            respond MediaRelease.findAll([sort:"dateCreated", order:"desc", max: 50]) {
                verified == true
                isMediaRelease == true
            }
        }
    }

    /**
     * The following method has been removed as the Crawler does not post to create new media
     * releases. However we're keeping the code in case the project is extended in the future.
     */

//    def save(MediaRelease mediaRelease){
//        // check that the media release is actually valid
//        if(!mediaRelease.hasErrors()){
//            def mediaReleaseInstance = mediaReleaseService.createMediaRelease(mediaRelease)
//            respond mediaReleaseInstance, status: 201
//        }
//        else{
//            respond mediaRelease
//        }
//
//    }
}
