package subscriberpoc

class SiteRestController {

    static responseFormats = ['json','xml']

    def index() {
        respond Site.all
    }
}
