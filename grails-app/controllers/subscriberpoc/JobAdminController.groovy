package subscriberpoc

import grails.plugin.springsecurity.annotation.Secured
import org.apache.commons.logging.LogFactory

@Secured(['ROLE_ADMIN'])
class JobAdminController {

    private static final log = LogFactory.getLog(this)
    def jobManagerService

    def index() {
        log.debug("Inside Job Admin Controller")
        redirect(action: 'show')
    }

    def show(){
        log.debug("Inside Show Job Admin Controller")
        def status = ""

        switch(params.operation){

            case 'Fire Crawler':
                status = "Started Crawler job"
                redirect(action: 'startCrawl')
                break

            case 'Pause Crawler':
                jobManagerService.pauseJobGroup("crawlServices")
                status = "Paused Crawler job"
                break
            case 'Resume Crawler':
                jobManagerService.resumeJobGroup("crawlServices")
                status = "Resumed Crawler job"
                break

            case 'Fire Emailer':
                status = "Started Crawler job"
                redirect(action: 'startEmailer')
                break

            case 'Fire Expirer':
                status = "Started Expirer job"
                redirect(action: 'startExpirer')
                break
        }


        return [status: status]
    }

    def startCrawl(){
        CrawlerJob.triggerNow()
        redirect(action: 'show')
    }

    def startEmailer(){
        EmailerJob.triggerNow()
        redirect(action: 'show')
    }

    def startExpirer(){
        PwdExpirerJob.triggerNow()
        redirect(action: 'show')
    }
}
