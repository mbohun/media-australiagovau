package subscriberpoc

import au.com.bytecode.opencsv.CSVWriter
import grails.plugin.springsecurity.annotation.Secured
import org.apache.commons.logging.LogFactory

@Secured(['ROLE_ADMIN'])
class StatisticsController {

    private static final log = LogFactory.getLog(this)

    def index() {

        def graphDataTtoS = [] //data points to be used in drawing the graph
        def graphDataColumnsTtoS = [['string', 'Topics'], ['number', 'Subscribers']] //types and labels for axes

        def allTopics = Topic.all
        for(Topic theTopic: allTopics) {
            def count = theTopic.subscribers.size()
            def newGraphData = [theTopic.name, count]
            graphDataTtoS.add(newGraphData);
        }



        def graphDataMRtoS = []
        def graphDataColumnsMRtoS = [['string', 'Site'], ['number', 'Media Releases']]
        def sitesWithOneOrLess = []
        def sitesLastUpdated = []
        def sites = Site.all
        for(Site site: sites) {
            def count = 0
            def totalCount = 0;
            def releases = site.releases
            def date = new Date(0L);
            releases.each({it3 ->
                totalCount += 1;
                if(it3.isMediaRelease) {
                    count += 1;
                    if(it3.dateCreated.after(date)) {
                        date = it3.dateCreated;
                    }
                }
            })
            def newGraphData = [site.url, count]
            graphDataMRtoS.add(newGraphData)
            if(count <= 1) {
                def siteOneOrLess = [site.url, count, totalCount, site.id]
                sitesWithOneOrLess.add(siteOneOrLess)
            }
            def lastUpdatedData = [site.url, date, totalCount, site.id]
            sitesLastUpdated.add(lastUpdatedData)
        }


        def graphDataMRtoT = []
        def graphDataColumnsMRtoT = [['string', 'Topics'], ['number', 'Media Releases']]

        for(Topic theTopic: allTopics) {
            def count = 0;
            def listOfSites = Site.where {
                topics.name == theTopic.name
            }


            listOfSites.each({it ->
                def releases = it.releases;
                releases.each({it2 ->
                    if(it2.isMediaRelease) {
                        count += 1;
                    }
                })
            });
            def newGraphData = [theTopic.name, count]
            graphDataMRtoT.add(newGraphData)
        }

        sitesWithOneOrLess.sort { a,b -> a[2] <=> b[2] };
        sitesWithOneOrLess.sort { a,b -> a[1] <=> b[1] };
        sitesLastUpdated.sort { a,b -> a[2] <=> b[2] };
        sitesLastUpdated.sort { a,b -> a[1] <=> b[1] };

        //add the following to the model; graphData and graphDataColumns are fed into google visualizations
        [graphDataTtoS: graphDataTtoS, graphDataColumnsTtoS: graphDataColumnsTtoS, graphDataMRtoS: graphDataMRtoS, graphDataColumnsMRtoS: graphDataColumnsMRtoS, graphDataMRtoT: graphDataMRtoT, graphDataColumnsMRtoT: graphDataColumnsMRtoT, sitesWithOneOrLess: sitesWithOneOrLess, sitesLastUpdated: sitesLastUpdated]

    }

    def export_subscriber_per_topic() {
        log.debug("Exporting Subscriber Per Topic")
        StringWriter sw = new StringWriter();
        CSVWriter writer = new CSVWriter(sw);
        String[] line = ["Topic", "Count"].toArray()
        writer.writeNext(line)

        def allTopics = Topic.all
        for(Topic theTopic: allTopics) {
            def count = theTopic.subscribers.size()
            line = [theTopic.name, count].toArray()
            writer.writeNext(line)
        }

        render(text: sw.toString(), contentType: 'text/csv')
    }

    def export_media_releases_per_site() {
        log.debug("Exporting Media Releases Per Site")
        StringWriter sw = new StringWriter();
        CSVWriter writer = new CSVWriter(sw);
        String[] line = ["Site", "Count"].toArray()
        writer.writeNext(line)
        def sites = Site.all
        for(Site site: sites) {
            def count = 0
            def totalCount = 0;
            def releases = site.releases
            def date = new Date(0L);
            releases.each({it3 ->
                totalCount += 1;
                if(it3.isMediaRelease) {
                    count += 1;
                    if(it3.dateCreated.after(date)) {
                        date = it3.dateCreated;
                    }
                }
            })
            line = [site.url, count].toArray()
            writer.writeNext(line)
        }


        render(text: sw.toString(), contentType: 'text/csv')
    }

    def export_media_releases_per_topic() {
        log.debug("Exporting Media Releases Per Topic")
        StringWriter sw = new StringWriter();
        CSVWriter writer = new CSVWriter(sw);
        String[] line = ["Topic", "Count"].toArray()
        writer.writeNext(line)
        def allTopics = Topic.all
        for(Topic theTopic: allTopics) {
            def count = 0;
            def listOfSites = Site.where {
                topics.name == theTopic.name
            }


            listOfSites.each({ it ->
                def releases = it.releases;
                releases.each({ it2 ->
                    if (it2.isMediaRelease) {
                        count += 1;
                    }
                })
            });
            line = [theTopic.name, count].toArray()
            writer.writeNext(line)
        }

        render(text: sw.toString(), contentType: 'text/csv')
    }

    def export_site_less_than_one_release() {
        log.debug("Exporting Site with Less than One Release")
        StringWriter sw = new StringWriter();
        CSVWriter writer = new CSVWriter(sw);
        String[] line = ["Site", "Count", "Total pages Crawled", "id"].toArray()
        writer.writeNext(line)
        def sites = Site.all
        for(Site site: sites) {
            def count = 0
            def totalCount = 0;
            def releases = site.releases
            def date = new Date(0L);
            releases.each({it3 ->
                totalCount += 1;
                if(it3.isMediaRelease) {
                    count += 1;
                    if(it3.dateCreated.after(date)) {
                        date = it3.dateCreated;
                    }
                }
            })
            if(count <= 1) {
                line = [site.url, count, totalCount, site.id].toArray()
                writer.writeNext(line)
            }
        }

        render(text: sw.toString(), contentType: 'text/csv')
    }

    def export_site_last_updated() {
        log.debug("Exporting Site Last Updated")
        StringWriter sw = new StringWriter();
        CSVWriter writer = new CSVWriter(sw);
        String[] line = ["Site", "Date", "Total pages Crawled", "id"].toArray()
        writer.writeNext(line)
        def sites = Site.all
        for(Site site: sites) {
            def count = 0
            def totalCount = 0;
            def releases = site.releases
            def date = new Date(0L);
            releases.each({it3 ->
                totalCount += 1;
                if(it3.isMediaRelease) {
                    count += 1;
                    if(it3.dateCreated.after(date)) {
                        date = it3.dateCreated;
                    }
                }
            })
            line = [site.url, date, totalCount, site.id].toArray()
            writer.writeNext(line)
        }

        render(text: sw.toString(), contentType: 'text/csv')
    }
}
