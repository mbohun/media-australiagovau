package subscriberpoc

class AgencyRestController {

    static responseFormats = ['json','xml']

    def index() {
        respond Agency.all
    }
}
