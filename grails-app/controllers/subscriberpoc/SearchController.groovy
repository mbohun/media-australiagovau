package subscriberpoc

import grails.plugin.springsecurity.annotation.Secured
import org.apache.commons.logging.LogFactory

class SearchController {

    private static final log = LogFactory.getLog(this)

    @Secured(['ROLE_ADMIN'])
    def index() {
        def originalQuery = request.getParameter("q")
        log.debug("Inside Search. Searching on query [" + originalQuery + "]")
        def query = '%' + originalQuery + '%'

        def sites = Site.findAllByUrlIlike(query)
        def sites2 = Site.findAllByReleaseUrlIlike(query)

        def subscribers = Subscriber.findAllByEmailIlike(query)

        def media_releases = MediaRelease.findAllByUrlIlike(query)
        def media_releases2 = MediaRelease.findAllBySnippetIlike(query)
        def media_releases3 = MediaRelease.findAllByTitleIlike(query)


        Topic foundTopic = Topic.findByName(originalQuery)
        def topicSites
        if(foundTopic != null) {
            topicSites = Site.findAll("from Site where ? in elements(topics)", [foundTopic])
        }


        sites.addAll(sites2)
        sites.unique()
        media_releases.addAll(media_releases2)
        media_releases.addAll(media_releases3)
        media_releases.unique()
        log.debug("Found [" + (sites.size()) + "] sites")
        log.debug("Found [" + subscribers.size() + "] subscribers")
        log.debug("Found [" + media_releases.size() + "] media_releases")
        if(topicSites != null) {
            log.debug("Found [" + topicSites.size() + "] sites")
        }
        [sites: sites, subscribers: subscribers, mediareleases: media_releases, topicSites: topicSites]
    }
}
