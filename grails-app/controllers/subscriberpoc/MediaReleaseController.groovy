package subscriberpoc

import grails.plugin.springsecurity.annotation.Secured
import org.apache.commons.logging.LogFactory

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import subscriberpoc.util.ShortURLCreator
import subscriberpoc.util.ShortURLCreatorBitly

@Transactional(readOnly = false)
class MediaReleaseController {

    private static final log = LogFactory.getLog(this)
    def springSecurityService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    static final String MRS_BITLY_ACCESS_TOKEN = System.getenv("MRS_BITLY_ACCESS_TOKEN")?: "Bitly access token not set!";
    static final ShortURLCreator shortUrlCreator = new ShortURLCreatorBitly(MRS_BITLY_ACCESS_TOKEN)

    def index(Integer max) {

        def list = MediaRelease.findAll([sort:"dateCreated", order:"desc", max: 90]) {
            verified == true
            isMediaRelease == true
        }
        params.max = 50
        respond list
    }

    @Secured(['ROLE_ADMIN'])
    def approve(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def notYetSent = MediaRelease.findAllWhere([hasBeenSent: false, verified: false])
        respond notYetSent, model: [mediaReleaseInstanceCount: notYetSent.size()]
    }

    @Secured(['ROLE_ADMIN'])
    def show(MediaRelease releaseInstance) {
        respond releaseInstance
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        respond new MediaRelease(params)
    }

    @Secured(['ROLE_ADMIN'])
    @Transactional
    def save(MediaRelease releaseInstance) {
        if (releaseInstance == null) {
            notFound()
            return
        }

        if (releaseInstance.hasErrors()) {
            respond releaseInstance.errors, view: 'create'
            return
        }

        log.debug("User [" + springSecurityService.principal.username + "] creating mediarelease [" + releaseInstance.title + "]")
        releaseInstance.shortUrl = shortUrlCreator.create(releaseInstance.url)
        releaseInstance.save flush: true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'release.label', default: 'Release'), releaseInstance.id])
                redirect releaseInstance
            }
            '*' { respond releaseInstance, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def edit(MediaRelease releaseInstance) {
        respond releaseInstance
    }

    @Secured(['ROLE_ADMIN'])
    @Transactional
    def update(MediaRelease releaseInstance) {
        if (releaseInstance == null) {
            notFound()
            return
        }

        if (releaseInstance.hasErrors()) {
            respond releaseInstance.errors, view: 'edit'
            return
        }

        log.debug("User [" + springSecurityService.principal.username + "] updating mediarelease [" + releaseInstance.title + "]")
        releaseInstance.shortUrl = shortUrlCreator.create(releaseInstance.url)
        releaseInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Release.label', default: 'Release'), releaseInstance.id])
                //redirect releaseInstance
                redirect action: "approve", method: "GET", controller: "mediaRelease"
            }
            '*' { respond releaseInstance, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def send() {

        if (params != null) {
            log.debug("Unmodified [" + params.release + "]")
            List selected = params.list('release')
            List allIds = params.list('releaseId')

            log.debug("User [" + springSecurityService.principal.username + "] sending out media releases")
            log.debug("[" + selected?.size() + "] confirmed releases to email out.")


            // mark all selected releases as mr's
            selected.each({it->
                def release = MediaRelease.findById(it)
                release.isMediaRelease = true
                release.save()
                log.debug("Send out in email [" + release.title + "] with id [" + release.id + "]")
            })

            // mark all viewed releases as verified
            allIds.each({it->
                def rel = MediaRelease.findById(it)
                rel.verified = true
                rel.save()
            })

            EmailerJob.triggerNow([releases: selected])

        }
        redirect action: "index"
    }

    @Secured(['ROLE_ADMIN'])
    @Transactional
    def delete(MediaRelease releaseInstance) {

        if (releaseInstance == null) {
            notFound()
            return
        }

        log.debug("User [" + springSecurityService.principal.username + "] deleting mediarelease [" + releaseInstance.title + "]")
        releaseInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Release.label', default: 'Release'), releaseInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'release.label', default: 'Release'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
