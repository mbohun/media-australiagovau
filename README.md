# SubscriberPOC

[![CircleCI](https://circleci.com/gh/AusDTO/media-australiagovau/tree/refactoring_migration.svg?style=svg&circle-token=6c15f52c849a206fa3f13746a43285f99c483dfd)](https://circleci.com/gh/AusDTO/media-australiagovau) 
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE) 

A simple proof of concept app to thrash out whether the issues involved in setting up a subscriber service

Beneficial to run with Java 7. On Mac:

Command to run web-app:
grails run-app

Command to run cron job:
grails run-script /var/www/SubscriberPOC/grails-app/utils/cron.groovy

### Refactoring/Migration

#### Intro (review & analysis)

Current env:

```
[root@FmrsSrv03Cbr01 ~]# uname -a
Linux FmrsSrv03Cbr01 3.10.0-229.14.1.el7.x86_64 #1 SMP Tue Sep 15 15:05:51 UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
```
```
[root@FmrsSrv03Cbr01 ~]# uptime
12:33:58 up 118 days, 2:12, 1 user, load average: 0.06, 0.04, 0.05
```
```
[root@FmrsSrv03Cbr01 ~]# cat /etc/*rel*
CentOS Linux release 7.1.1503 (Core)
Derived from Red Hat Enterprise Linux 7.1 (Source)
NAME="CentOS Linux"
VERSION="7 (Core)"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="7"
PRETTY_NAME="CentOS Linux 7 (Core)"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:centos:centos:7"
HOME_URL="https://www.centos.org/"
BUG_REPORT_URL="https://bugs.centos.org/"

CENTOS_MANTISBT_PROJECT="CentOS-7"
CENTOS_MANTISBT_PROJECT_VERSION="7"
REDHAT_SUPPORT_PRODUCT="centos"
REDHAT_SUPPORT_PRODUCT_VERSION="7"

cat: /etc/prelink.conf.d: Is a directory
CentOS Linux release 7.1.1503 (Core)
CentOS Linux release 7.1.1503 (Core)
cpe:/o:centos:centos:7
```
```
[root@FmrsSrv03Cbr01 ~]# free -m
total used free shared buff/cache available
Mem: 3950 1567 384 197 1997 1913
Swap: 2047 102 1945
```
- possible problem: size of RAM is 4gb, but SWAP size is only 2gb; solution: size of SWAP partition or swap file should be at least 8gb in my honest and conservative opinion (i am going to analyze/confirm the RAM and disk space requirements in my dev env); NOTE: from the disk space info in the next section bellow we already see that the disk space is only 12gb and 7.1gb is already used, clearly too small for an extra 8gb of swap file (or at least extra 6gb of swap file that would add to the existing 2gb of swap)
- i will do my best to optimize/scale the RAM/SWAP/disk space requirements to the possible minimum, if the further analysis confirms that the app can be safely run with 2gb RAM and 4gb SWAP we can easily scale it down; an alternative approach is to use separate env/instances: one for running jvm + tomcat + mediareleases war, and second/separate env for mysql, etc.
- TODO: i am going to extend this section once we receive more details about the current prod env, for example is the env used by other tomcat applications, is the mysql db used by other app, etc. 

```
[root@FmrsSrv03Cbr01 ~]# df -hT
Filesystem Type Size Used Avail Use% Mounted on
/dev/vda1 ext3 12G 7.1G 4.1G 64% /
devtmpfs devtmpfs 2.0G 0 2.0G 0% /dev
tmpfs tmpfs 2.0G 0 2.0G 0% /dev/shm
tmpfs tmpfs 2.0G 201M 1.8G 11% /run
tmpfs tmpfs 2.0G 0 2.0G 0% /sys/fs/cgroup
```
- see the above section note about the size of RAM and SWAP space

```
[svc_fin_mrs_p_app@FmrsSrv03Cbr01 ~]$ java -version
java version "1.8.0_25"
Java(TM) SE Runtime Environment (build 1.8.0_25-b17)
Java HotSpot(TM) 64-Bit Server VM (build 25.25-b02, mixed mode)
```

- the existing apps's README.md (this file, at the very top) says to use jdk1.7.x, **BUT:**
- according to the [official tomcat7 install guide](https://tomcat.apache.org/tomcat-7.0-doc/appdev/installation.html) "Tomcat 7.0 was designed to run on Java SE 6.", **HOWEVER** according to a [diff apache tomcat doco](https://tomcat.apache.org/whichversion.html) jdk1.7.x is fine.
- the existing app uses grails 2.4.4; although the initial versions/releases of grails 2.4.x were compatible with jdk1.8.x, this compatibility was apparently broken when grails starting form some later version/release of grails 2.4.x started using some undocumented features of jdk1.8.x. when these undocumented featues were removed/changed from later versions of jdk1.8.x, grails 2.4.x became **INCOMPATIBLE** with jdk1.8.x
- similar caution should be exercised with all the other components (like tomcat, etc.), and we should at this stage strictly use ONLY the versions of the componenents the existent app uses, the version numbers of these components are available from ./grails-app/conf/BuildConfig.groovy from the `dependencies` and `plugins` sections, for example the tomcat version is 7.0.55
- although make sure tomcat actually does use the version of java you want, for example I have recently started tomcat (7.0.70) and tomcat ignored my JAVA_HOME, resp used that to determine it is "Oracle" java, and the rest was pointing to openjdk1.8 jre in /usr/lib64, to enforce the java version you want create `$CATALINA_HOME/bin/setenv.sh` script:
  ```bash
  cat setenv.sh
  #!/bin/bash

  JAVA_HOME=/usr/local/jdk
  JRE_HOME=$JAVA_HOME/jre
  ```

#### SQL DB
```
    -- MySQL dump 10.14  Distrib 5.5.44-MariaDB, for Linux (x86_64)
    --
    -- Host: localhost    Database: db1
    -- ------------------------------------------------------
    -- Server version	5.5.44-MariaDB-log

    /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
    /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
    /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
    /*!40101 SET NAMES utf8 */;
    /*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
    /*!40103 SET TIME_ZONE='+00:00' */;
    /*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
    /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
    /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
    /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
   
```

- from the provided sql dump we see the db engine is `5.5.44-MariaDB`, for Linux (x86_64)
- disk space (we were given sql dump of the current prod env, but the size of the dump seems to suspiciously small when compared to number of records we retrieved today (2016-06-21) through the web interface (today's volume of data was **roughly estimated** to be between **20-60mb** compared to the **7-8mb** size of the sql dump from (2016-06-16); for example could it be that the sql dump was created just after some cleanup/expiry of records, and since then new records were added to the db?
- db installation for now with ansible oe shell script into a vm
- from the sql dump make a db creation script (handy for testing)
- TODO: old prod -> new prod db migration


#### CircleCI

**NOTE:** Before the existing application was evaluated/tested for security issues we should **NOT** be exposing the war file publicly on the Internet! For example if the war will be stored in a maven repo, the maven repo should be either protected with username/password, or over some ssh/vpn like connection, etc.

- maven repo (sonatatype nexus, or artifactory)
 - [sonatatype nexus](https://github.com/mbohun/travis-build-configuration/blob/master/doc/Sonatype-nexus-notes.md)
 - [artifactory](https://inthecheesefactory.com/blog/how-to-setup-private-maven-repository/en)
- [maven repo in amazon s3 bucket](https://github.com/mbohun/travis-build-configuration/blob/master/doc/maven-on-amazon-S3-bucket.md)
- https://jitpack.io
- directly by `cf push`
- for a limited/restricted access one could use any maven repo accessible only from within some VPN/subnet NOT accessible to the outside world

NOTE: The prod env installation should allow for a simple rollback, reverting/reinstalling previous (latest stable) version in case there was a problem.

#### PCF
- http://cloud.rohitkelapure.com/2015/10/tuning-default-memory-setting-of-java.html
- https://github.com/dmikusa-pivotal/cf-debug-tools#logs

#### Running the application
- running `grails run-app` defaults to `development` env/configuration, i.e. `$APP/grails-app/conf/Config.groovy` `environments.development` settings will be used, to override this behavior on runtime see the next point.
- `grails -Dgrails.env=mycustomenv run-app`; `grails.env` can be used to dynamically control/change the apps sttings/configuration without a need to rebuild the war file according to https://grails.org/wiki/Environments
- TODO: test if something like `grails -Dgrails.env.development.grails.mail.port=25 run-app` works and can be used override properties, in this real case/example the SMTP port number is set to 1025 in grails code, however my test env is already running a mail server on the standard port 25

#### REST API
- the application is at the moment exposing (**subset** of it's functionality in form of a) REST API
- judging from the following [URL mappings](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/conf/UrlMappings.groovy#L13-L16); one can assume at least some REST functionality to be available for:
 - sites https://media.australia.gov.au/api/sites
 - mediareleases https://media.australia.gov.au/api/mediareleases
 - ~~subscribers https://media.australia.gov.au/api/subscribers~~ NOTE: does **NOT** work and/or requires authentication, etc. ?
 - topics https://media.australia.gov.au/api/topics

example of the REST API use from cli/script env:

```BASH
mbohun@linux-9gpe:~/src/media-australiagovau.git> curl -s https://media.australia.gov.au/api/topics
[{"id":1,"name":"Benefits and Payments","description":"Aged care, carers, crisis, disability, family, indigenous, jobseekers, veterans, youth payments and services"},{"id":2,"name":"Business and Industry","description":"ABN, grants, non-profit and small business, primary industry, import and export, science and technology, tenders"},{"id":3,"name":"Culture and Arts","description":"Indigenous heritage and history, arts grants, cultural institutions, heritage, history, honours and awards, family history"},{"id":4,"name":"Education and Training","description":"Early childhood, school, higher education, international students, skills recognition, scholarships, vocational"},{"id":5,"name":"Environment","description":"Energy efficiency, environmental management and protection, biodiversity, grants, natural resources"},{"id":6,"name":"Family and Community","description":"Births, deaths and marriages, child care, housing and property, relationships, social issues"},{"id":7,"name":"Health","description":"Workplace health and safety, drug and alcohol use, health promotion, health insurance, disability insurance, childrens' health, mental health, sport"},{"id":8,"name":"Immigration and Visas","description":"Australian citizenship, customs, work, study and short-term visas, migration and tourism"},{"id":9,"name":"IT and Communications","description":"Data, internet, postal services, television and radio"},{"id":10,"name":"Jobs and Workplace","description":"Careers, government jobs, employment services, disbility employment, working conditions"},{"id":11,"name":"Money and Tax","description":"Tax returns, ABN, superannuation, personal finance, financial regulation"},{"id":12,"name":"Passports and Travel","description":"Customs and quarantine, embassies and consulates, travelling overseas"},{"id":13,"name":"Public Safety and Law","description":"Consumer protection, online safety, emergency services, legislation, police, rights"},{"id":14,"name":"Security and Defence","description":"National security, cyber security, ADF, military history, commemoration"},{"id":15,"name":"Transport and Regional","description":"Registration and licences, roads and transport, aviation, regional development"},{"id":16,"name":"About Government","description":"Government services, parliament, international relations, Indigenous affairs, elections, matters of state"},{"id":17,"name":"About Australia","description":"Climate, holidays, events, statistics, Australian history and culture"}]
```
```JSON
mbohun@linux-9gpe:~/src/media-australiagovau.git> curl -s https://media.australia.gov.au/api/topics | python -m json.tool
[
    {
        "description": "Aged care, carers, crisis, disability, family, indigenous, jobseekers, veterans, youth payments and services",
        "id": 1,
        "name": "Benefits and Payments"
    },
    {
        "description": "ABN, grants, non-profit and small business, primary industry, import and export, science and technology, tenders",
        "id": 2,
        "name": "Business and Industry"
    },
    {
        "description": "Indigenous heritage and history, arts grants, cultural institutions, heritage, history, honours and awards, family history",
        "id": 3,
        "name": "Culture and Arts"
    },

...

    {
        "description": "Climate, holidays, events, statistics, Australian history and culture",
        "id": 17,
        "name": "About Australia"
    }
]
```
```BASH
mbohun@linux-9gpe:~/src/media-australiagovau.git> curl -s https://media.australia.gov.au/api/topics | python -m json.tool | grep name
"name": "Benefits and Payments"
"name": "Business and Industry"
"name": "Culture and Arts"
"name": "Education and Training"
"name": "Environment"
"name": "Family and Community"
"name": "Health"
"name": "Immigration and Visas"
"name": "IT and Communications"
"name": "Jobs and Workplace"
"name": "Money and Tax"
"name": "Passports and Travel"
"name": "Public Safety and Law"
"name": "Security and Defence"
"name": "Transport and Regional"
"name": "About Government"
"name": "About Australia"

mbohun@linux-9gpe:~/src/media-australiagovau.git> curl -s https://media.australia.gov.au/api/topics | python -m json.tool | grep description
"description": "Aged care, carers, crisis, disability, family, indigenous, jobseekers, veterans, youth payments and services",
"description": "ABN, grants, non-profit and small business, primary industry, import and export, science and technology, tenders",
"description": "Indigenous heritage and history, arts grants, cultural institutions, heritage, history, honours and awards, family history",
"description": "Early childhood, school, higher education, international students, skills recognition, scholarships, vocational",
"description": "Energy efficiency, environmental management and protection, biodiversity, grants, natural resources",
"description": "Births, deaths and marriages, child care, housing and property, relationships, social issues",
"description": "Workplace health and safety, drug and alcohol use, health promotion, health insurance, disability insurance, childrens' health, mental health, sport",
"description": "Australian citizenship, customs, work, study and short-term visas, migration and tourism",
"description": "Data, internet, postal services, television and radio",
"description": "Careers, government jobs, employment services, disbility employment, working conditions",
"description": "Tax returns, ABN, superannuation, personal finance, financial regulation",
"description": "Customs and quarantine, embassies and consulates, travelling overseas",
"description": "Consumer protection, online safety, emergency services, legislation, police, rights",
"description": "National security, cyber security, ADF, military history, commemoration",
"description": "Registration and licences, roads and transport, aviation, regional development",
"description": "Government services, parliament, international relations, Indigenous affairs, elections, matters of state",
"description": "Climate, holidays, events, statistics, Australian history and culture",
```

#### CrawlerService implementation notes

- Mediareleases uses [crawler4j](https://github.com/yasserg/crawler4j) version [4.1](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/conf/BuildConfig.groovy#L57).
- In it's current version crawler4j forces you to use the filesystem: [config.setCrawlStorageFolder(crawlStorageFolder)](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/services/subscriberpoc/CrawlerService.groovy#L58); you have to provide a name of a directory for temporary/working files carawler4j will create and use
- In it's current (most likely not the most efficient) implementation Mediareleases [for each site](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/services/subscriberpoc/CrawlerService.groovy#L37):  
 1. [creates new CrawlConfig](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/services/subscriberpoc/CrawlerService.groovy#L57)
 2. [sets storage/directory on the filesystem](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/services/subscriberpoc/CrawlerService.groovy#L58)
 3. [sets resumable crawling to false](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/services/subscriberpoc/CrawlerService.groovy#L65) **THIS IS IMPORTANT** because of [this](https://github.com/yasserg/crawler4j/blob/crawler4j-4.1/src/main/java/edu/uci/ics/crawler4j/crawler/CrawlController.java#L106) crawler4j will clean up (delete the content) the storage/directory on the filesystem before crawling each site
 4. [sets number of crawlers to 1](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/services/subscriberpoc/CrawlerService.groovy#L56)
 5. [starts the crawler](https://github.com/AusDTO/media-australiagovau/blob/master/grails-app/services/subscriberpoc/CrawlerService.groovy#L74)

NOTE: crawler4j uses Berkeley DB Java Edition version [5.0.73](https://github.com/yasserg/crawler4j/blob/master/pom.xml#L135); this version 5.0.73 is ["No longer available for download."](http://www.oracle.com/technetwork/database/database-technologies/berkeleydb/downloads/index-098622.html); Unfortunately this dependency on Berkeley DB Java Edition complicates changes/customization of crawler4j if required.
