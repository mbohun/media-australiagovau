#!/bin/bash

# do a REST call to get the data for MRS sites
SITES_JSON=`curl -s https://media.australia.gov.au/api/sites`

# extract the list of base URLs (shaving off all the crap from JSON) and store them in an array
# so now we have an array like: http://minister.agriculture.gov.au, http://minister.agriculture.gov.au, etc.
declare -a base_url_array=(`echo $SITES_JSON | python -m json.tool | grep '\"url\"\:' | sed -e 's/^.*\"url\"\: \"//g' -e 's/\/\"$/\"/g' -e 's/\"$//g'`)

# extract the list of MRS locations (shaving off all the crap from JSON) and store them in an array
# so now we have an array like: /joyce/Pages/Media-Releases/, /ruston/Pages/Media-Releases/
declare -a mrs_location_array=(`echo $SITES_JSON | python -m json.tool | grep '\"releaseUrl\"\:' | sed -e 's/^.*\"releaseUrl\"\: \"//g' -e 's/\"\,$//g'`)

# get the length of the array we are going to iterate with an index
array_length=${#base_url_array[@]}

#TODO: error handling make sure BOTH arrays are of the same length (they have to be)

for (( i=0; i<${array_length}; i++ ));
do
    # concatinate the base URL + MRS location (http://minister.agriculture.gov.au + /joyce/Pages/Media-Releases/) dumping it to the stdout
    echo "${base_url_array[$i]}${mrs_location_array[$i]}"
done
