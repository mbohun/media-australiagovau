import requests
import json

def json_array_to_map(arr, key_name):
    lookup = {}
    for item in arr:
        k = item[key_name]
        del(item[key_name])
        lookup[k] = item
    return lookup

def load_rest_data(rest_api_endpoint):
    rest_max = 50 # TODO: currently hardcoded to 50 in grails mediareleases REST controller
    rest_offset = 0
    arr = []

    while True:
        http_res = requests.get(rest_api_endpoint + "?offset=" + str(rest_offset))
        data = json.JSONDecoder().decode(http_res.text)
        arr.extend(data)
        if len(data) < rest_max:
            break
        rest_offset += rest_max
    return arr

if __name__=="__main__":

    agencies_http_res = requests.get("https://mediareleases.apps.staging.digital.gov.au/api/agencies")
    agencies_lookup = json_array_to_map(json.JSONDecoder().decode(agencies_http_res.text), 'id')

    topics_http_res = requests.get("https://mediareleases.apps.staging.digital.gov.au/api/topics")
    topics_lookup = json_array_to_map(json.JSONDecoder().decode(topics_http_res.text), 'id')

    sites_http_res = requests.get("https://mediareleases.apps.staging.digital.gov.au/api/sites")
    sites_lookup = json_array_to_map(json.JSONDecoder().decode(sites_http_res.text), 'id')

    mediareleases_arr = load_rest_data("https://mediareleases.apps.staging.digital.gov.au/api/mediareleases")
    mediareleases_lookup = json_array_to_map(mediareleases_arr, 'id')

    for k in sites_lookup.keys():
        agency_id = sites_lookup[k]['agency']['id']
        sites_lookup[k]['agency']['title'] = agencies_lookup[agency_id]['title']

        topics_arr = sites_lookup[k]['topics']
        for topic in topics_arr:
            topic_id = topic['id']
            topic['name'] = topics_lookup[topic_id]['name']

        releases_arr = sites_lookup[k]['releases']
        for release in releases_arr:
            release_id = release['id']
            release['url'] = mediareleases_lookup[release_id]['url']

    print json.dumps(sites_lookup)
