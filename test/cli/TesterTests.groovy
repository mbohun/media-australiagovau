import grails.test.AbstractCliTestCase

class TesterTests extends AbstractCliTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testTester() {

        execute(["tester"])

        assertEquals 0, waitForProcess()
        verifyHeader()
    }
}
