package subscriberpoc
import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(SubscriberRestController)
@Mock([Subscriber, Topic])
class SubscriberRestControllerSpec extends Specification {

    void setupSpec(){
        defineBeans{
            springSecurityService(SpringSecurityService)
        }
    }

    void "GET a list of subscribers"(){
        given: "A set of subscribers"
        initialiseTestData()

        when: "I invoke the index action"
        controller.index()

        then: "I get a JSON list of subscribers"
        response.status == 200
        response.json*.email.sort() == [
                "dean@test.com"
        ]
    }

//    void "POST a single subscriber as JSON"(){
//        given: "A set of existing subscribers"
//        initialiseTestData()
//        Subscriber next = new Subscriber(email: 'yoyoma@itunes.com')
//        next.addToTopics(new Topic(name: 'Voilins', description: 'Walls I fall off'))
//
//
//
//
//        when: "I invoke the save action with a JSON packet"
//        request.json = next
//        controller.save()
//
//        then: "I get a 201 JSON response"
//        response.status == 201
//        response.json.id != null
//        response.email == "ratt@blah.com"    // should fail here
//
//    }


    private initialiseTestData(){
        Subscriber newSub = new Subscriber(email: 'dean@test.com', confirmCode: '1234')
        newSub.addToTopics(new Topic(name: 'Humpty Dumptys', description: 'Walls I fall off'))
        newSub.save(failOnError: true)

        return newSub.id
    }
}


