package subscriberpoc

import grails.test.mixin.*
import org.apache.commons.logging.LogFactory
import spock.lang.*

@TestFor(SubscriberController)
@Mock([Subscriber, Topic])


class SubscriberControllerSpec extends Specification {
    private static final log = LogFactory.getLog(this)
    def setup(){


    }
    def populateValidSubscriberParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        params["id"] = 50
        params["version"] = 0
        params["email"] = 'test@test.com'
        params["confirmCode"] = 'test_confirmcode'
        params["verified"]= 'false'

    }
    def populateValidTopicParams (params){
        params["id"] = 50
        params["version"] = 0
        params["description"] = "Test"
        params["name"] = "Test Topic"

    }


    void "Test the signup action correctly persists an instance"() {

        when:"The update action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def subscriber = new Subscriber()
        populateValidTopicParams (params)
        log.debug("Params Id "+params.id)
        controller.params.topics=params.id
        subscriber.validate()
        controller.update(subscriber)

        then:"The modify view is rendered again with the correct model"
        model.subscriberInstance!= null
        view == 'edit'

        when:"The save action is executed with a valid instance"
        response.reset()
        populateValidTopicParams(params)
        populateValidSubscriberParams(params)
        subscriber = new Subscriber(params)
        controller.params.topics=params.id
        controller.update(subscriber)

        then:"A redirect is issued to the show action"
        URLDecoder.decode(response.redirectedUrl, "UTF-8") == "/subscriber/showSubscriber?email=$subscriber.email"
        controller.flash.message != null
        log.debug("subscriber 50 "+Subscriber.findAllById(50))
        Subscriber.count() == 1
    }
    /**
    void "Test the update action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def agency = new Agency()
        agency.validate()
        controller.save(agency)

        then:"The create view is rendered again with the correct model"
        model.agencyInstance!= null
        view == 'create'

        when:"The save action is executed with a valid instance"
        response.reset()
        populateValidParams(params)
        agency = new Agency(params)

        controller.save(agency)

        then:"A redirect is issued to the show action"
        response.redirectedUrl == '/agency/1'
        controller.flash.message != null
        Agency.count() == 1
    }**/
}