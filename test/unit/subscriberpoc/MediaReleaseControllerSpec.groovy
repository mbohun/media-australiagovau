package subscriberpoc



import grails.test.mixin.*
import spock.lang.*

@TestFor(MediaReleaseController)
@Mock(MediaRelease)
class MediaReleaseControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        params["id"]=1
        params["version"]=0
        params["is_media_release"] = 'true'
        params["site_id"]=2
        params["snippet"]="Test snippet for media release"
        params["title"]='Test'
        params["url"]="http://www.test.com.au"
        params["dateCreated"]=new Date()
        params["releaseDate"]=new Date()
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.releaseInstanceList
            model.releaseInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.mediaReleaseInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def release = new MediaRelease()
            release.validate()
            controller.save(release)

        then:"The create view is rendered again with the correct model"
            model.mediaReleaseInstance!= null
            view == 'create'

    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def release = new MediaRelease(params)
            controller.show(release)

        then:"A model is populated containing the domain instance"
            model.mediaReleaseInstance == release
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def release = new MediaRelease(params)
            controller.edit(release)

        then:"A model is populated containing the domain instance"
            model.mediaReleaseInstance == release
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/mediaRelease'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def release = new MediaRelease()
            release.validate()
            controller.update(release)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.mediaReleaseInstance == release

    }
}
