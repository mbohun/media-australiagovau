package subscriberpoc

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(StatisticsController)
@Mock([Subscriber, Topic, User, Site, Role, UserRole, Agency, MediaRelease])
class StatisticsControllerSpec extends Specification {
    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        params["email"] = 'test@test.com'
        params["confirmCode"] = 'test_confirmcode'
        params["verified"] = 'false'
        params["topics"]=[Topic.findById(1), Topic.findById(2)]

    }
    void "Test the index action returns the correct model"() {

        when: "The index action is executed"
            def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
            def first = new User(username: "Pablo", password: 'password', email: "pablo@test.com").save()
            UserRole.create first, adminRole, true
            Topic benefits = new Topic(name: 'Benefits and Payments', description: 'Aged care, carers, crisis, family, indigenous, jobseekers, veterans, youth payments and services').save()
            Topic business = new Topic(name: 'Business and Industry', description: 'ABN, grants, non-profit and small business, primary industry, import and export, science and technology, tenders').save()
            def transport = new Topic(name: 'Transport and Regional', description: 'Registration and licences, roads and transport, aviation, regional development').save()
            Agency agriculture = new Agency(title: "Dept of Agriculture").save()
            agriculture.addToSites(new Site(created: "created", description: "description", url: "http://www.agricultureminister.gov.au/Pages/Media-Releases.aspx", createdRegex: "yyyy-MM-dd-'T'HH:mm", mediaReleaseSelector: "h1:eq(Media Release)")).save()
            MediaRelease mr1 = new MediaRelease(dateCreated: new Date(), isMediaRelease: true, url: "http://www.agricultureparlsec.gov.au/Pages/Media-Releases/recognising-the-value-of-rec-fishing-in-aus.aspx", title: "Recognising the value of recreational fishing in Australia", snippet: "Parliamentary Secretary to the Minister for Agriculture, Senator Richard Colbeck, will this weekend attend the National Recreational Fishing Conference and Tournament on the Gold Coast where he will speak about the Government's commitment to the sector. The Government is working to further strengthen lines of communication with our recreational fishers � who are estimated to contribute... Senator Colbeck said. ", releaseDate: new Date(), site: Site.findById(1)).save()
            def third = new Subscriber(email: "mediareleasetester@gmail.com", verified: true, confirmCode: "a90ab7d7-31ef-41b0-b814-bee216bc1436", topics: [Topic.findById(1), Topic.findById(2)] ).save()

            controller.index()

        then: "The values to be displayed exist"
            controller.index().values()!=0
    }
}